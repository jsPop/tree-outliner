<?php

APP::import('model', 'IndexedItem');
APP::import('model', 'Tag');
APP::import('model', 'ItemTag');
APP::import('model', 'ItemList');

/**
 *
 * @property IndexedItem $IndexedItem
 * @property Tag $Tag
 * @property ItemTag $ItemTag
 *
 * Class Item
 *
 *
 */
class Item extends AppModel {

    public $actsAs = array('Containable');

    public $hasOne = array('IndexedItem' => array(
        'foreignKey' => 'item_id',
    ));

    function __construct() {
        parent::__construct();

        $this->Tag = new Tag();
        $this->IndexedItem = new IndexedItem();
        $this->ItemTag = new ItemTag();
        $this->List = new ItemList();

        $this->List->contain(array());

        $this->CON_maxTTL = 600;
        $this->CON_baseRand = 500000;
        $this->CON_baseRandDelta = $this->CON_baseRand / 10;
        $this->CON_randLimit = 3372036854775807;
    }

    function getItems($listId, $userId, $getDef = false) {
        $this->contain(array(
            'IndexedItem' => array(
                'fields' => array(
                	'user_id',
                	'next_review',
                	'time_type',
                	'review_type',
                	'review_times',
					'channel'
				),
                'conditions' => array(
                    'IndexedItem.user_id' => $userId,
                    'IndexedItem.removed' => null,
                )
            )
        ));

        $conds = array(
            'Item.list_id' => $listId,
            'Item.removed' => null,
        );

        if ($getDef) {
            $conds[] = 'Item.def is not null';
        }

        return $this->find('all', array(
            'conditions' => &$conds
        ));
    }

	function syncAt($raw, $timestamp, $listId, $userHash, $today, $opts = array()) {
		$items = $this->parseReqForSync($raw, $timestamp);

		$onlySeqChangedKeys = 'id,id_seq,seq,synced';

        $indexedItems = array();

		foreach($items as &$item) {
            $itemData = &$item['Item'];
			$itemKeys = array_keys($itemData);
			sort($itemKeys);
			$keys = join(',', $itemKeys);
//			$this->l('keys at here', array('keys' => $keys, 'itemKeys' => $itemKeys));
			if ($keys == $onlySeqChangedKeys) $itemData['updated'] = false;
			if (isset($itemData['removed']) && $itemData['removed']) {
				$itemData['removed'] = date('Y-m-d H:i:s');
			}
			$itemData['list_id'] = $listId;
			$itemData['user_hash'] = $userHash;
            $itemData['spent_date'] = $today;

            if (isset($itemData['indexed'])) {
                if ($itemData['indexed']) {
                    $indexedItems[] = array(
                        'id' => $itemData['id'],
                        'tags' => $itemData['tags'],
                    );
                } else {
                    $indexedItems[] = array(
                        'id' => $itemData['id'],
                        'removed' => true
                    );
                }
            }
		}

//		$this->l($items);
		$this->saveAll($items);
        $randRangeInfo = $opts['randRangeInfo'] ? $opts['randRangeInfo'] : $this->getRandRangeInfo($opts['userId']);
        $this->indexItems($indexedItems, $opts['userId'], $randRangeInfo, $opts['now']);
	}

    var $CON_maxTTL;
    var $CON_baseRand;
    var $CON_baseRandDelta;
    var $CON_randLimit;

    public function _time() {
        return floor(microtime(true) * 1000);
    }

    public function searchIndexedItems($userId, $term = '', $channelText) {
    	$this->contain(array());

    	$db = $this->getDataSource();

		if (strlen(trim($term)) > 0) {
			$singleTerms = preg_split('!\s+!', trim($term));
			$and = array();
			$num = 0;
			foreach($singleTerms as $t) {
				$termPart = $this->getDataSource()->value('%' . $t . '%');
				$and[] = "Item.content like $termPart";
			}
		}

		if (!$channelText) {
			$and[] = "(indexed.channel is null or indexed.channel = '')";
		}
		else if ($channelText != 'all') {
			$and[] = "indexed.channel = " . $this->getDataSource()->value($channelText) . "";
		}


    	$sql = "select Item.*,
    	indexed.next_review, indexed.time_type, indexed.review_type,
    	indexed.review_times, indexed.channel,
    	list.title as list_title
    	from tr_items as Item
    	left join tr_indexed_items as indexed on indexed.item_id = Item.id
    	left join tr_item_lists as list on Item.list_id = list.id
    	where indexed.user_id = $userId and indexed.removed is null
    		and Item.removed is null
    	";

    	if (count($and)) {
    		$sql .= ' and ' . implode($and, ' and ');
    	}

    	$sql .= ' order by next_review limit 80';

//    	echo $term . '           ';

    	$items = $db->fetchAll($sql);

		$itemRes = array();
    	foreach($items as &$item) {
    		$itemRecord = TrUtils::pick($item['Item'], array(
    			'content', 'id'
    		));

    		TrUtils::transfer($item['indexed'], $itemRecord, array(
    			'next_review', 'time_type', 'review_type', 'review_times',
				'channel'
    		));

    		$itemRecord['list_title'] = $item['list']['list_title'];

            $itemRes[] = &$itemRecord;
    		unset($itemRecord);
    	}

    	return $itemRes;
    }

    public function getReviewItems($num, $userId, $tags, $opts = array()) {

        $this->contain(array());

        $db = $this->getDataSource();

        $sql = "select Item.*,
        indexed.ttl, indexed.rand_seq,
        indexed.next_review, indexed.review_times, indexed.importance, indexed.created, indexed.reviewed,
        indexed.review_type
        from tr_items as Item
        left join tr_indexed_items as indexed on indexed.item_id = Item.id ";

        $statSql = "select count(1) as item_count
        from tr_items as Item
        left join tr_indexed_items as indexed on indexed.item_id = Item.id ";

        $onVals = array();
        $whereVals = array();

        if ($tags) {
            $tagSql = "";
            $tagWhere = array();

            foreach ($tags as $i => $tag) {
                $tagSql .= " left join tr_item_tags as it$i on it$i.item_id = Item.id and it$i.user_id = ? ";
                $tagWhere[] = " it$i.tag_id = ? ";
                $onVals[] = $userId;
                $whereVals[] = $tag;
            }

            $tagWhere = implode('and', $tagWhere);

            $sql .= " $tagSql where $tagWhere and ";
            $statSql .= " $tagSql where $tagWhere and ";
        } else {
            $sql .= ' where ';
            $statSql .= ' where ';
        }

        $sql .= " indexed.user_id = ? and (indexed.next_review is null or indexed.next_review < ?)
            and indexed.removed is null and Item.removed is null
            order by rand_seq limit $num";

        $statSql .= " indexed.user_id = ? and (indexed.next_review is null or indexed.next_review < ?)
            and indexed.removed is null and Item.removed is null
            order by rand_seq limit 1000";

        $whereVals[] = $userId;
        $whereVals[] = $opts['now'];

//        try {
            $items = $db->fetchAll($sql, array_merge($onVals, $whereVals));
//        } catch (Exception $ex) {}


        $itemRes = array();
        $itemRandIds = array();
        $listIds = array();
        $itemIds = array();
        $now = time();
        $ret = array();
        $randSeqTooBig = false;
        if (!$opts['randRangeInfo']) {
            $randRangeInfo = $this->getRandRangeInfo($userId);
        } else {
            $randRangeInfo = $opts['randRangeInfo'];
        }

        // to update the min number
        $isFirst = true;

        $fields = array_merge($this->CON_itemFields, array('list_id'));
        foreach($items as &$item) {
            $itemRecord = TrUtils::pick($item['Item'], $fields);
            $listIds[$item['Item']['list_id']] = 1;
            $this->l('before - ', $itemRecord);
            TrUtils::transfer($item['indexed'], $itemRecord, array(
                'next_review', 'review_times', 'importance', 'created', 'reviewed',
                'review_type'
            ));
            $this->l('after - ', $itemRecord);
            $this->l('indexed - ', $item['indexed']);
            $itemRes[] = &$itemRecord;

            $itemRandIds[] = $item['Item']['id'];
            $itemIds[] = $item['Item']['id'];
            $parentIds[] = $item['Item']['parent_item_id'];

            if ($isFirst) {
                if ($item['indexed']['rand_seq'] > $randRangeInfo['min'])
                    $randRangeInfo['min'] = $item['indexed']['rand_seq'];
                $isFirst = false;
            }
            if (!$randSeqTooBig && $item['indexed']['rand_seq'] > $this->CON_randLimit) {
                $randSeqTooBig = true;
            }
            unset($itemRecord);
        }

        $listIds = array_keys($listIds);

        if ($randSeqTooBig) {
//            $this->l('rand_seq', array($item['indexed']['rand_seq'], $randSeqTooBig));
            // renew all rand_seq when the rand_seq is too big
            $this->resetAllRand($userId);
            $randRangeInfo = 0;
        } else {

            // update item rand when item is not too old
            if ($itemRandIds) {
                $max = $this->setRand($itemRandIds, $userId, $now, $randRangeInfo);
                $randRangeInfo['max'] = $max > $randRangeInfo['min'] ? $max : $randRangeInfo['min'];
            }
        }

        $ret['items'] = &$itemRes;
        $ret['arounds'] = $this->getAroundItems($itemIds, $parentIds);
        $ret['lists'] = $this->getLists($listIds);

        $count = $db->fetchAll($statSql, array_merge($onVals, $whereVals));
        $ret['count'] = intval($count[0][0]['item_count']);

        $this->l('review items -', $ret);
//        $log = $this->getDatasource()->getLog(false, false);
//        $this->l('patch sql log - ', $log);

        $ret['randRangeInfo'] = $randRangeInfo;
        return $ret;
    }

    public function getIndexedItems($itemNum, $userId, $onlyNonActioned = true) {
        $sql = "select Item.*, IndexedItem.channel from tr_items as Item
            left join tr_indexed_items as IndexedItem on IndexedItem.item_id = Item.id
            where (IndexedItem.has_actioned = 0 or IndexedItem.has_actioned is null)
            and IndexedItem.user_id = ? and IndexedItem.removed is null
            order by IndexedItem.updated desc
            limit $itemNum";
        $db = $this->getDataSource();
        $allItems = $db->fetchAll($sql, array($userId));

        return $allItems;
    }

    public function setIndexedItem($userId, $indexedItem) {
    	$this->IndexedItem->save(
    		array(
    			'review_times' => $indexedItem['review_times'],
    			'review_type' => $indexedItem['review_type'],
    			'time_type' => $indexedItem['time_type'],
    			'next_review' => $indexedItem['next_review'],
    			'reviewed' => $indexedItem['now'],
    			'channel' => $indexedItem['channel'],
    			'user_id' => $userId,
    			'item_id' => $indexedItem['id'],
    		)
    	);
    }

    public function review($itemId, $reviewType, $nextReview, $userId, $now, $newReview = true) {
        $this->IndexedItem->save(
            array(
                '' => array(
                    'review_times' => ($newReview ? 'coalesce(review_times, 0) + 1' : 'coalesce(review_times, 0)'),
                ),
                'review_type' => $reviewType,
                'next_review' => $nextReview,
                'reviewed' => $now,
                'user_id' => $userId,
                'item_id' => $itemId,
            )
        );
    }

    public function getLists($listIds) {

        $res = $this->List->find('all', array(
            'fields' => array(
                'title', 'id'
            ),
            'conditions' => array(
                'id' => $listIds
            )
        ));

        $lists = array();
        foreach ($res as $listRec) {
            $lists[] = TrUtils::pick($listRec['ItemList'], array(
                'title', 'id'
            ));
        }

        return $lists;
    }

    public function resetAllRand($userId) {
        $this->IndexedItem->updateAll(array(
            'rand_seq' => '(FLOOR(RAND() * ' . $this->CON_baseRand . ' ))'
        ), array(
            'removed is null',
            'user_id' => $userId
        ));
    }

    public function getAroundItems($itemIds, $parentIds) {
        $res = $this->find('all', array(
            'conditions' => array(
                'removed is null',
                'OR' =>
                array(
                    'parent_item_id' => $itemIds,
                    'id' => $parentIds
                )
            )
        ));

        $arounds = array();
        foreach($res as &$item) {
            $arounds[] = TrUtils::pick($item['Item'], $this->CON_itemFields);
        }
//        $this->l('parents -', $res);

        return $arounds;
    }

    public function getRandRangeInfo($userId) {
        $res = $this->IndexedItem->find('first', array(
            'fields' => array('max(rand_seq) as max', 'min(rand_seq) as min'),
            'conditions' => array(
                'removed is null',
                'user_id' => $userId
            )
        ));

        $max = $res[0]['max'];
        $min = $res[0]['min'];

        return array('max' => $max ? $max : 0,
                'min' => $min ? $min : 0);
    }

    public function setRand($itemIds, $userId, $now, $randRangeInfo) {

        $this->IndexedItem->updateAll(array(
            'rand_seq' => "FLOOR(RAND() *
                ((${randRangeInfo['max']}  - ${randRangeInfo['min']}) * 4 / 5 + {$this->CON_baseRand})
                + ${randRangeInfo['min']} + (${randRangeInfo['max']} - ${randRangeInfo['min']}) / 5)",
            "ttl" => $now + $this->CON_maxTTL
        ), array(
            'user_id' => $userId,
            'item_id' => $itemIds
        ));

        $res = $this->IndexedItem->find('first', array(
            'fields' => array('max(rand_seq) as max'),
            'conditions' => array(
                'item_id' => $itemIds,
                'user_id' => $userId,
            )
        ));

        $max = $res[0]['max'];
        $this->l('set rand - ', $itemIds);
        return $randRangeInfo['max'] > $max ? $randRangeInfo['max'] : $max;
    }

//    public function resetRand($itemIds, $userId, $range, $now) {
//        $this->IndexedItem->updateAll(array(
//            'rand_seq' => "${range['min']} + FLOOR(RAND() * (${range['max']} - ${range['min']}))",
//            "ttl" => $now + $this->CON_maxTTL
//        ), array(
//            'user_id' => $userId,
//            'item_id' => $itemIds
//        ));
//        $this->l('reset rand - ', $itemIds);
//    }


    public function indexItems($indexedItems, $userId, $randRangeInfo, $now) {

        $this->l('rand range at sync - ', $randRangeInfo);

        /*
         * #items: [] => { id: '', tags: [], removed: true | false }
         */
        $items = array();
        $itemTags = array();
        $itemIds = array();
        $userTags = array();
        $itemsForTags = array();

        foreach ($indexedItems as $item) {

            if (!$item['removed']) {
                $items[] = array(
                    'item_id' => $item['id'],
                    'user_id' => $userId,
                    'removed' => null,
                    '' => array(
                        'rand_seq' => "FLOOR(RAND() *
                            (${randRangeInfo['max']} - ${randRangeInfo['min']}))
                            + ${randRangeInfo['min']}",
                        'created' => "coalesce(created, $now)",
                    )
                );
                $itemIds[] = $item['id'];


                if (isset($item['tags'])) {
                    foreach ($item['tags'] as $tag) {
                        if (!isset($tagHash[$tag])) {
                            $userTags[] = array(
                                'tag_id' => $tag,
                                'user_id' => $userId
                            );
                        }

                        $itemTags[] = array(
                            'tag_id' => $tag,
                            'item_id' => $item['id'],
                            'user_id' => $userId,
                        );
                    }
                    $itemsForTags[] = $item['id'];
                }
            } else {
                $items[] = array(
                    'item_id' => $item['id'],
                    'removed' => date('Y-m-d H:i:s'),
                    'user_id' => $userId,
                );
            }
        }

        $this->ItemTag->deleteAll(array('item_id' => $itemsForTags, 'user_id' => $userId), false);
        $this->Tag->saveAll($userTags);
        $this->IndexedItem->saveAll($items);
        $this->ItemTag->saveAll($itemTags);

        $log = $this->getDataSource()->getLog(false, false);
        $this->l('sql for list', $log);
    }

	function loadUpdatePatch($lastSynced, $listId, $userHash, $getDef = false) {
		$itemRec = $this->find('all', array(
				'conditions' => array(
					'user_hash !=' => $userHash,
					'list_id' => $listId,
					'synced >' => $lastSynced,
                    $getDef ? 'def is not null' : '',
				),
		));

//		$this->l('update patch', $itemRec);
// 		$log = $this->getDataSource()->getLog(false, false);
// 		$this->l('patch sql log - ', $log);

		$patch = $this->parsePatchForRsp($itemRec);
		return $patch;
	}

	function parsePatchForRsp($itemRec) {
		$patch = array();

		foreach($itemRec as $item) {
            $patch[] = $this->parseItem($item['Item']);
		}
		return $patch;
	}

    var $CON_itemFields = array(
        'id', 'content', 'parent_item_id',
        'user_hash', 'type', 'removed', 'task_status',
        'is_hidden', 'task_points', 'ref_item_id', 'status',
        'synced', 'seq', 'user_hash', 'updated',
        'task_spent', 'spent_date', 'def',
		'created_by', 'updated_by'
    );

    function parseItem(&$item) {
        $itemData = TrUtils::pick($item, $this->CON_itemFields);

        return $itemData;
    }

	function parseReqForSync($raw, $timestamp) {
		$items = array();

		// __todo: remove sensitive cols - e.g. owner
		// __todo: only the author/people who are shared with can modify
		foreach($raw as &$rawItem) {
			$rawItem['synced'] = $timestamp;
			// __todo: test item list
			$items[] = array('Item' => $rawItem);
		}
		return $items;
	}

    function addBackLink($listId, $backLinkId, $backLinkTitle, $time, $backLinkText = 'List created from') {
        $itemId = uniqid(substr(MD5(microtime().uniqid()), 0, 5));
        $data = array(
            'Item' => array(
                'id' => $itemId,
                'list_id' => $listId,
                'seq' => -100,
                'content' => "$backLinkText @{" . $backLinkId . "||$backLinkTitle}@",
                'synced' => $time,
                'user_hash' => 'auto'
            )
        );

//        $this->l($data);

        $this->save($data);
        return $itemId;
    }

    function moveToList($itemId, $synced, $listId, $opts = array()) {
        $data = array(
            'Item' => array(
                'id' => $itemId,
                'list_id' => $listId,
                'synced' => $synced,
                'seq' => $opts['seq'],
            )
        );

        if ($opts['isDirectChild']) {
            $data['Item']['parent_item_id'] = $opts['parentItem'];
        }

        $this->save($data);
    }

	function maxId() {
		$itemMaxId = $this->query('select max(id_seq) as max from tr_items');
//		$this->l('item max id', $itemMaxId);
		$max = $itemMaxId[0][0]['max'];
		$prts = explode('_', $max);
		return $prts[0];
	}
}
