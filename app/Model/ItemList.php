<?php

/**
 * Class ItemList
 * @property Item Item
 */
class ItemList extends AppModel
{
	public $actsAs = array('Containable');

	public $hasMany = array('Item' => array(
		'foreignKey' => 'list_id',
	));

	public $hasOne = array('ViewedList' => array(
		'foreignKey' => 'list_id'
	));

	function getListInfo($id)
	{
		$this->contain(array());
		$listRec = $this->find('first', array(
			'conditions' => array(
				'ItemList.id' => $id
			)
		));
//		$this->l('list rec info only - ', $listRec);
		return $listRec['ItemList'];
	}

	function processLightItemList($listRec)
	{
		$return = array();
		foreach ($listRec as $rec) {
			$return[] = $rec['Item'];
		}

		return $return;
	}

	function getRecentRemoved($listId, $lastTimeAnchor)
	{
		if (!$lastTimeAnchor) {
			$listRec = $this->Item->find('all', array(
				'conditions' => array(
					'AND' => array(
						'Item.removed is not null',
						'Item.content is not null',
						'Item.list_id' => $listId
					)
				),
				'order' => 'Item.removed desc',
				'limit' => 300
			));

			return $this->processLightItemList($listRec);
		}
	}

	function getRecentUpdated($listId, $lastTimeAnchor)
	{
		$listRec = $this->Item->find('all', array(
			'conditions' => array(
				'AND' => array(
					'Item.removed is null',
					'Item.list_id' => $listId
				)
			),
			'order' => 'Item.updated desc',
			'limit' => 300
		));

		return $this->processLightItemList($listRec);
	}

	function removeList($listId)
	{
		$now = date('Y-m-d H:i:s');

		$this->updateAll(array(
			'removed' => "'$now'"
		),
			array(
				'ItemList.id' => $listId
			)
		);
	}

	function revertRemoved($listId, $removedAt)
	{
		$now = date('Y-m-d H:i:s');

		$this->Item->updateAll(array(
			'removed' => null,
			'updated' => "'$now'"
		),
			array(
				'Item.list_id' => $listId,
				'Item.removed' => $removedAt
			)
		);
	}

	function getList($id, $userId = false, $getDef = false)
	{
		$this->contain($this->getContains($userId, $getDef));

		$listRec = $this->find('first', array(
			'conditions' => array(
				'ItemList.id' => $id,
				'ItemList.removed' => null,
			)
		));

		$items = $this->Item->getItems($id, $userId, $getDef);

		$this->l('item rec', $items);
		$list = $this->processListForRsp($listRec, $userId, $items);

//		$log = $this->getDataSource()->getLog(false, false);
//		$this->l('sql for list', $log);

		return $list;
	}

	function search($term, $userId)
	{
		$this->contain();

		$singleTerms = preg_split('!\s+!', trim($term));
		$and = array();
		$num = 0;
		foreach ($singleTerms as $t) {
			$and[str_repeat(' ', $num++) . 'title like'] = "%$t%";
		}

		$listRecs = $this->find('all', array(
			'fields' => array('owner_id', 'title', 'id', 'ViewedList.fav_seq'),
			'conditions' => array(
				'AND' => array(
					'ItemList.removed is null',
					'OR' => array(
						'ViewedList.user_id' => $userId,
						'ViewedList.user_id is null'
					),

					// 尼玛, 注意 'OR ' 后的那个空格!!!
					'OR ' => array(
						'ItemList.owner_id ' => $userId,


						// 额... share_type 后必须要有一个空格, 否则 key 会被覆盖
						"ItemList.share_type " => "public",
						"ItemList.share_type" => "readonly",
						'AND ' => array(
							"ItemList.share_type" => "semi-public",
							"ViewedList.user_id is not null"
						)
					),
					$and
				)
			),
			'joins' => array(
				array(
					'table' => 'tr_viewed_lists',
					'alias' => 'ViewedList',
					'type' => 'LEFT',
					'conditions' => array(
						'ViewedList.list_id = ItemList.id',
						'ViewedList.user_id' => $userId
					)
				)
			),
			'order' => 'ViewedList.fav_seq desc, ItemList.synced desc',
			'limit' => 120
		));

//		$this->l('list recs for search - ', $listRecs);


		$lists = array();
		foreach ($listRecs as &$item) {
			$listRec = &$item ['ItemList'];
			$lists [] = array(
				'id' => $listRec ['id'],
				'title' => $listRec ['title'],
				'fav_seq' => $item['ViewedList']['fav_seq'] == null ? false : true
			);

			unset($listRec);
		}

		$log = $this->getDataSource()->getLog(false, false);
//		print_r($log);

		return $lists;
	}

	function getContains($userId = false)
	{
		$contains = array();

		if ($userId) {
			$contains ['ViewedList'] = array(
				'conditions' => array(
					'user_id' => $userId
				)
			);
		}
		return $contains;
	}

	function dirtyList($listId, $synced)
	{
		$list = array(
			'ItemList' => array(
				'id' => $listId,
				'synced' => $synced
			)
		);
		$this->save($list);
	}

	function dirtyListUpdatedByItemId($itemId)
	{
		$now = date('Y-m-d H:i:s');

		$itemRec = $this->Item->find('first', array(
			'conditions' => array(
				'id' => $itemId
			)
		));

		$this->updateAll(
			array('ItemList.updated' => "'$now'"),
			array('ItemList.id' => $itemRec['Item']['list_id'])
		);
	}

	function dirtyListUpdated($listId)
	{
		$now = date('Y-m-d H:i:s');

		$this->updateAll(
			array('ItemList.updated' => "'$now'"),
			array('ItemList.id' => $listId)
		);
	}

	function favDoc($userId, $listId, $isFav)
	{
		$this->ViewedList->updateAll(
			array(
				'fav_seq' => $isFav === 'true' ? 1 : null
			),
			array(
				'user_id' => $userId,
				'list_id' => $listId
			)
		);

		$this->dirtyListUpdated($listId);
	}

	function moveItems($listId, $synced, $children, $items, $opts = array())
	{
		$seq = 9999;
		if ($children) {
			foreach ($children as $child) {
				$this->Item->moveToList($child, $synced, $listId, array(
					'isDirectChild' => true,
					'parentItem' => $opts['parentItem'],
					'seq' => $seq++
				));
			}
		}

		if ($items) {
			foreach ($items as $item) {
				$this->Item->moveToList($item, $synced, $listId, array(
					'seq' => $seq++
				));
			}
		}
	}


	function createList($userId, $time, $title = '未命名文档')
	{
		$list = array(
			'ItemList' => array(
				'id' => uniqid(substr(MD5(microtime() . uniqid()), 0, 5)),
				'owner_id' => $userId,
				'title' => $title,
				'synced' => $time,
			));
		$this->save($list);
		return $this->getList($this->id, $userId);
	}

	function processListForRsp($listRec, $userId, $items)
	{
		$list = TrUtils::pick($listRec['ItemList'],
			array('id', 'created', 'updated', 'owner_id', 'title', 'list_type', 'share_type'));
		$list['items'] = array();

		foreach ($items as &$item) {
			$itemRes = $this->Item->parseItem($item['Item']);
			$itemRes['indexed'] = !!$item['IndexedItem']['user_id'];
			$itemRes['next_review'] = $item['IndexedItem']['next_review'];
			$itemRes['time_type'] = $item['IndexedItem']['time_type'];
			$itemRes['review_type'] = $item['IndexedItem']['review_type'];
			$itemRes['review_times'] = $item['IndexedItem']['review_times'];
			$itemRes['channel'] = $item['IndexedItem']['channel'];
			$list['items'][] = $itemRes;
		}

		if (!$listRec['ViewedList']['item_status']) {
			$list['itemStatus'] = '';
		} else {
			$list['itemStatus'] = $listRec['ViewedList']['item_status'];
		}

		$list['fav_seq'] = $listRec['ViewedList']['fav_seq'] == null ? false : true;

		if (!$listRec['ViewedList']['id']) {
			$this->ViewedList->saveStatus($list['id'], $userId, "");
		}
		$this->l($listRec);

		return $list;
	}
}
