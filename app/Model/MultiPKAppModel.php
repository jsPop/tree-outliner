<?php
/**
 * Created by IntelliJ IDEA.
 * User: lzheng
 * Date: 12/11/2014
 * Time: 7:26 PM
 */

class MultiPKAppModel extends AppModel {

    public function saveAll($data = array(), $options = array()) {
        foreach($data as $record) {
            $this->save($record);
        }
    }

    public function save($record = NULL, $validate = true, $fieldList = array()) {
        $keys = array();
        $vals = array();
        $keyPairs = array();
        $places = array();

        foreach($record as $k => $v) {

            if (!$k) {
                foreach ($v as $rawK => $rawV) {
                    $keys[] = $rawK;
                    $places[] = $rawV;
                    $keyPairs[] = "$rawK=$rawV";
                }
            } else {
                $keys[] = $k;
                $vals[] = $v;
                $places[] = '?';
                $keyPairs[] = "$k=?";
            }
        }

        $vals = array_merge($vals, $vals);
        $keys = implode(',', $keys);
        $places = implode(',', $places);
        $keyPairs = implode(',', $keyPairs);
        $db = $this->getDataSource();

        $db->fetchAll("insert into {$this->tablePrefix}{$this->useTable} ($keys)
                values ($places) ON DUPLICATE KEY UPDATE $keyPairs", $vals);
    }
} 