<?php
/**
 * Created by IntelliJ IDEA.
 * User: lzheng
 * Date: 21/11/2014
 * Time: 8:48 PM
 */

class MyObject extends AppModel {

    function getObjects($ids) {
        $objRecs = $this->find('all', array(
            'conditions' => array(
                'id' => $ids
            )
        ));

        $objs = array();
        foreach($objRecs as $objRec) {
            $objs[$objRec['MyObject']['id']] = $objRec['MyObject'];
        }

        return $objs;
    }

    function saveObject($raw) {
        unset ($raw['created']);
        unset ($raw['updated']);

        $object = array(
            'MyObject' => $raw
        );

        $this->save($object);
    }

    function search($name, $space, $offset, $step, $tokens = array()) {

        $conds = array();
        if ($name) {
            $conds['_name_ like'] = "$name%";
        }
        if ($space) {
            $conds['_space_ like'] = "$space%";
        }

        $objRecs = $this->find('all', array(
            'conditions' => &$conds,
            'order' => 'updated desc',
            'offset' => $offset,
            'limit' => $step
        ));

        $results = array();
        foreach($objRecs as $objRec) {
            $results[] = $this->parseObject($objRec, $tokens);
        }

        return $results;
    }

    function parseObject(&$objectRecord, &$tokens) {
        $obj = &$objectRecord['MyObject'];
        if ($tokens[$obj['_token_']]) {
            $obj['_owned'] = true;
        }
        unset($obj['_token_']);
        return $obj;
    }
} 