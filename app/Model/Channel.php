<?php

/**
 *
 * Class Channel
 *
 *
 */
class Channel extends AppModel
{

	function __construct()
	{
		parent::__construct();

	}

	function getChannels($userId)
	{
		$rec = $this->find('all', array(
			'conditions' => array(
				'owner_id' => $userId
			)
		));

		return $rec;
	}

	function addChannel($userId, $channelText)
	{
		$rec = $this->find('first', array(
			'conditions' => array(
				'owner_id' => $userId,
				'label_text' => $channelText
			)
		));

		if (!count($rec)) {
			$this->save(array(
				'owner_id' => $userId,
				'label_text' => $channelText,
				'is_displayed' => 't'
			));
		} else if ($rec[0]['Channel']['is_displayed'] != 't') {
			$this->updateAll(
				array('is_displayed' => "'t'"),
				array(
					'owner_id' => $userId,
					'label_text' => $channelText
				)
			);
		}
	}

	function removeChannel($userId, $channelText)
	{
		$this->deleteAll(array(
			'owner_id' => $userId,
			'label_text' => $channelText
		));
	}

	function removeDisplayed($userId, $channelText)
	{
		$this->updateAll(
			array('is_displayed' => "'f'"),
			array(
				'owner_id' => $userId,
				'label_text' => $channelText
			)
		);
	}
}
