<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<title>
		<?php echo $title_for_layout; ?>
	</title>
</head>
<body class="<?php if (isset($css)) echo $css; ?>">

<!--<div>Loading the page. Please wait ... ^ ^</div>-->
    <?php if (!$isPopup) { ?>

        <link href="/css/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="/css/vex.css" rel="stylesheet">
        <script src="/js/require.js" type="text/javascript"></script>

        <style>
            <?php echo $extraStyles ?>
        </style>
        <script>
            require.config({
                urlArgs : "bust=v" + 15
                /** <DEBUG do not touch this kind of comments* */
                    + (new Date()).getTime()
                /** DEBUG>* */
            });
        </script>
    <?php
echo $this->fetch ( 'meta' );
echo $this->App->less ( 'common.less', $appVersion );
echo $this->fetch ( 'css' );
echo $this->fetch ( 'script' );
?>

	<div id="page">
	<?php  echo $this->fetch('content'); ?>
	</div>
	<script type="text/javascript">
	var pv = <?php if (isset($pv)) { echo json_encode($pv); } else { echo '{}'; }?>;
	</script>
    <?php } ?>
</body>
</html>
