<?php
$this->Html->less('the-less.less', $appVersion, array('block' => 'css'));


$this->Html->script ('pages/the-script.js?ver=' . $appVersion, array(
    'block' => 'script'
));
?>
<!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">-->
<header>
<!-- Header if any -->
</header>
<section id="content">
<!--    Content of the page-->
</section>

<div id="hidden-zone" class="hidden">
<!--    Some hidden contents-->
</div>


<footer>
<!--    Footer if any-->
</footer>
