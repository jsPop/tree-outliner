<?php
$this->Html->script('pages/user.js',
		array(
		'block' => 'script'
	));
?>
<header id="header" role="banner">
	<nav class="aui-header aui-dropdown2-trigger-group" role="navigation">
		<div class="aui-header-inner">
			<div class="aui-header-primary">
				<h1 id="logo" class="aui-header-logo">
					<a class="" href="/"> <span
						class="fa fa-th-list outliner-logo"></span><span>BEFE 树状文档</span>
					</a>
				</h1>
			</div>
		</div>
	</nav>
</header>
<section id="content" role="main">
	<div class="aui-page-panel">
		<div class="aui-page-panel-inner">
			<section class="aui-page-panel-content">
				<h2>注册新账号</h2>

				<form method="post" class="aui top-label">
					<div id="errors"></div>
					<div class="field-group">
						<label>用户</label> <input id="user" type="text" name="reg-user"
							autocomplete="off" class="text long-field" />
						<div class="description">用户账号应只包含小写英文和数字</div>
					</div>
					<input style="display:none">
					<div class="field-group">
						<label>密码</label> <input id="pass" type="password" name="reg-pass"
							autocomplete="off" class="text long-field" />
					</div>
					<div class="field-group">
						<label>密码确认</label> <input id="confirm" type="password" name="reg-confirm"
						 	autocomplete="off" class="text long-field" />
					</div>
					<div class="buttons-container">
						<div class="buttons">
							<input class="aui-button aui-button-primary" type="submit" name="submit" value="注册" />
							<a class="aui-button aui-button-link" href="/login">取消</a>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
</section>
