---list-dialog

本快捷键打开文档列表对话框, 文档列表对话框, 可以用于快速跳转文档, 或者新建一个新的文档.

<video src="/quick-help/list-dialog.mp4"
	controls loop autoplay
></video>

---create-dialog

本快捷键打开文档列表对话框, 输入新文档的名称, 并点击新建 (或在没有其他文档选中的情况下, 按回车) 即可新建一个文档.

<video src="/quick-help/list-dialog.mp4"
	   controls loop autoplay
></video>


---help-dialog

本快捷键打开快速帮助.

<video src="/quick-help/help-dialog.mp4"
	   controls loop autoplay
></video>

---edit-current-item

本快捷键使当前选中节点进入编辑模式, 在输入框中输入节点新内容.输入完毕,点击编辑框后面的'√' 或 cmd+回车 即可保存修改.

<video src="/quick-help/edit-cur-item.mp4"
	   controls loop autoplay
></video>

---del-current-item

本快捷键将删除当前选中的节点, 节点删除后可在 '侧边栏->最近删除' 列表中看到(也可以撤销删除操作)

<video src="/quick-help/del-cur-item.mp4"
	   controls loop autoplay
></video>

