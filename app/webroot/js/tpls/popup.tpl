---spentItem
<div class="item" style="height: <%- d.height %>px;">
    <a class="block <%= d.cls %>" style="background-color: <%- d.color %>;
        border-color:<%- d.color %>;"
        title="<div class='tipsy-left'>List: <%- d.listTitle %><br/>
            Item: <%- d.itemTitle %><br/>
            Time: <%- d.time %></div>"
        <% if (d.url) { %>
            href="<%- d.url %>" target="<%- d.listMD5 %>"
        <% } %>
        ></a>
    <div class="text" style="font-size: <%= d.fontSize %>px"><%- d.title %></div>
</div>

---empty
<div class="empty" style="height: <%- d.height %>px">
</div>

---hourMarker
<div class="marker <%= d.half ? 'half' : '' %>" style="top: <%= d.height %>%">
    <div class="time-text"><%= d.text %></div>
    <div class="line"></div>
</div>

---task
<div class="row task" data-item="<%- d.itemId %>">
    <label title="<%- d.title %>"><%- d.title %></label>
    <span class="links">
        <span class="aui-button aui-button-link" data-finish-restart="5m">5m</span>
        <span class="aui-button aui-button-link" data-finish-restart="10m">10m</span>
        <span class="aui-button aui-button-link" data-finish-restart="20m">20m</span>
        <span class="aui-button aui-button-link" data-finish-restart="30m">30m</span>
        <span class="aui-button aui-button-link" data-finish-restart="1h">1h</span>
        <span class="aui-button aui-button-link" data-finish-restart="2h">2h</span>
        <span class="aui-button aui-button-link" data-finish-restart="3h">3h</span>
        <span class="aui-button aui-button-link" data-finish-restart="6h">6h</span>
    </span>
</div>