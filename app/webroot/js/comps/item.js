define(['ajax', 'jquery', 'utils', 'underscore',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'tab-indent', 'quick-help'],
	function (ajax, $, utils, _,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, tabIndent, quickHelp,
			  _undefined) {

		var isFirefox = typeof InstallTrigger !== 'undefined';

		// __logic: * Item
		var Item = Backbone.View.extend({
			className: 'item-wrapper',
			tagName: 'li',

			// __logic: item.events
			events: {
				'keydown > .item textarea': 'onTextareaKeydown',

				// __m21: detecting # and provide tag autocomplete
				'keyup > .item textarea': 'onTextareaTag',
				'click > .item textarea': 'onTextareaTag',

				'paste > .item textarea': 'onTextareaPaste',
				'click > .item .save': 'onSaveClick',
				'click > .item .cancel': 'onCancelClick',
				'click > .item .twix': 'onToggleExpanded',
				'click > .item .task-status': 'onToggleTaskStatus',
				'click > .item .inner-link': 'gotoInnerLink',
				'click > .item .embedded-link': 'gotoListItemLink',
				'click > .item .list-item-link': 'gotoListItemLink',
				'click > .item .running': 'gotoTimer',
				'click > .item .edit-item': 'onEdit',
				'click > .item .remove-item': 'removeItem',
				'click > .item .share-item': 'shareItem',
				'click > .item .toggle-selection': 'onToggleSelect',

				'click .field': 'onFieldClicked',

				// __m21: hover & leave tag
				// need to disable the hovering on tags in input
				'mouseenter #tag-input [tag-text]': 'cancelHoveringOnTag',
				'mouseleave #tag-input [tag-text]': 'cancelHoveringOnTag',
				'mouseenter #tag-input': 'hoverOnTagInput',
				'mouseleave #tag-input': 'leaveTagInput',
				'mouseenter [tag-text]': 'hoverOnTag',
				'mouseleave [tag-text]': 'leaveTag'
			},

			onFieldClicked: function (e) {
				e.stopPropagation();

				$(e.currentTarget).toggleClass('hidden-field');
			},

			gotoTimer: function () {
				utils.openTimer();
			},

			stopPropagation: function (e) {
				e.stopPropagation();
			},

			initialize: function (opts) {

				var t = this;
				_.extend(t, opts);
				if (!opts.item) return;

				if (!t.item.id) {
					t.item.id_seq = t.g.idSeq;
					t.item.id = t.getId();
				}
				t.resetOld();

				t.$el.data('view', t);

				if (!t.status) t.status = {};

				if (!t.item.children || !t.item.children.length) {
					t.$el.addClass('no-child');
				}

				if (!t.status.expanded) {
					t.$el.addClass('is-collapsed');
				}

				// __m18: logic - item-id for item
				t.$el.attr('item-id', t.item.id);

				// __m21: define $tagInput
				t.$tagInput = $('#tag-input');

				// __m21: define hidden zone
				t.$hiddenZone = $('#hidden-zone');

				t.render();
				t.initEvents();
			},

			initEvents: function () {
				var t = this;

				// __m22: event: catch the export & import event
				t.on('export-data', function (items) {
					t.exportData(items);
				})
					.on('import-data', function () {
						t.importData();
					})

			},

			// __m22: method: exportData
			exportData: function (items) {
				var t = this;

				if (items) {
					var data = t.getExportData(items);
				} else {
					data = t.getExportData(t);
				}

				var $dialog = dialog.open({
					className: 'prompt-dialog vex-theme-wireframe',
					message: t.g.tpls.exportData(data),
					buttons: [
						{
							text: 'Close',
							type: 'button',
							click: function () {
								vex.close($dialog.data('vex').id)
							}
						}
					],
					afterClose: function () {
						Mousetrap.unpause();
					}
				});

				$dialog.find('textarea').select();
			},

			getExportData: function (items) {
				if (!(items instanceof Array)) {
					items = [items];
				}
				var data = [];
				_.each(items, function (item) {
					item._getItemData(data);
				});
				return JSON.stringify(data);
			},

			importFromString: function (string, opts) {
				var t = this;
				opts = opts || {seq: 9999}

				try {
					var idMappings = {};
					var data = JSON.parse(string);
					if (!(data instanceof Array)) return;

					_.each(data, function (item) {
						var newId = t.getId();
						idMappings[item.id] = newId;
						item.id = newId;
						item.isDirty = true;
						item.seq = opts.seq++;
						if (!item.parent_item_id) {
							item.parent_item_id = t.item.id;
						} else {
							item.parent_item_id = idMappings[item.parent_item_id];
						}
					});
					t.g.list.applyPatch(data, true);
					t.dirty();
				} catch (ex) {
				}
			},

			_getItemData: function (data) {
				var t = this,
					item = {
						content: t.item.content,
						id: t.getId()
					};
				data.push(item);

				var $children = t.$('> .children > li');


				$children.each(function () {
					var child = $(this).data('view');
					if (child) {
						var childItem = child._getItemData(data);
						childItem.parent_item_id = item.id;
					}
					;
				});

				return item;
			},

			// __m22: method: importData
			importData: function () {
				// please paste the exported data and press ok
				var result = prompt('Please paste the exported data and press ok. Empty or invalid data will do nothing');
				var t = this;

				if (result) {
					t.importFromString(result);
				}
			},

			rgxHeading: /^<(h[1234])>/,
			checkHeading: function (bodyHtml) {
				var t = this, heading = t.rgxHeading.exec(bodyHtml);
				if (heading) {
					t.$el.attr('heading', heading[1]);
				} else {
					t.$el.removeAttr('heading');
				}
			},

			// __m18: __m18: gotoInnerLink()
			gotoInnerLink: function (e) {
				var t = this, $link = $(e.currentTarget), href = $link.prop('href');

				if ($link.data('id')) {
					if (e.metaKey || e.ctrlKey || e.altKey) {
						t.g.list.openSubContent(null, $link.data('id'), null, 'new');
					}
					else {
						t.g.list.openSubContent(null, $link.data('id'));
					}
				} else {
					if (href && href.indexOf(t.g.app.listUrl) == 0) {
						var itemId = href.substr(t.g.app.listUrl.length);
						t.g.list.hoverOnItem($('[item-id=' + itemId + ']'), true, {
							permaLink: true
						});
						e.preventDefault();
					}
				}

				e.stopImmediatePropagation();
			},

			gotoListItemLink: function (e) {
				if (e.metaKey || e.ctrlKey || e.altKey) {
					return;
				}

				var t = this, $link = $(e.currentTarget), href = $link.prop('href');
				if (t.g.list.openSubContent(href)) {
					e.preventDefault();
				}
			},

			// __logic: item.render
			render: function () {
				var t = this;

				if (t.item.isNew) {
					t.$el.html(t.g.tpls.item({
						statusHtml: t.getStatus(),
						bodyHtml: '请鼠标单击选中这个内容节点, 用快捷键 &lt;E> 开始编辑!'
					}))
				} else {
					var bodyHtml = t.getBody(t.item.content, t.item);
					t.$el.html(t.g.tpls.item({
						statusHtml: t.getStatus(),
						bodyHtml: bodyHtml
					}));
				}
				t.$el.toggleClass('empty-list', !t.item.content);
				t.checkHeading(bodyHtml);
				t.parseTags();
				t.enhanceEmbeddeds();
				t.setIndexedStyle();
			},

			getStatus: function () {
				return this.g.tpls.itemStatus();
			},

			// __logic: item.onToggleExpanded
			onToggleExpanded: function (e, expanded) {
				var t = this;
				if (typeof expanded != 'boolean') {
					// 如果没有传入, 则反转一下值
					expanded = !t.status.expanded;
				}
				t.status.expanded = expanded;
				t.$el.toggleClass('is-collapsed', !expanded);

				// __event: item.save-item-status
				t.trigger('save-item-status', t.status);
				e && e.stopPropagation();
			},

			//__logic: item.getBody
			getBody: function (externalContent, item) {
				return utils.getBody(externalContent, item);
			},

			parseTags: function ($tmp) {
				utils.parseTags($tmp || this.$('> .item'));
			},

			refresh: function () {
				var t = this;
				t.refreshBody();
			},

			toggleHeading: function (type) {
				var t = this;

				if (t.item.content) {
					if (t.item.content.indexOf('# ') === 0) {
						t.item.content = t.item.content.substr(2);
					}
					else {
						t.item.content = '# ' + t.item.content;
					}

					t.dirty();
					t.refreshBody();
				}
			},

			// __logic: item.refreshBody
			refreshBody: function () {
				var t = this, bodyHtml = t.getBody(t.item.content, t.item);
				var $body = t.$('> .item .body');

				if (t.g.list.isLearningMode) {
					var fieldsMap = {};
					$body.find('.field')
						.each(function () {
							var $field = $(this);
							fieldsMap[$field.html()] = $field.hasClass('hidden-field');
						});
				}

				$body.html(bodyHtml);

				if (t.g.list.isLearningMode) {
					$body.find('.field')
						.each(function () {
							var $field = $(this);

							if (fieldsMap[$field.html()]) {
								$field.addClass('hidden-field');
							}
						});
				}

				t.$el.toggleClass('empty-list', !t.item.content);
				t.checkHeading(bodyHtml);
				t.parseTags();
				t.enhanceEmbeddeds();

				t.setIndexedStyle();
			},

			toggleIndexed: function (indexed) {
				var t = this;
				if (typeof indexed === 'undefined') {
					indexed = !t.item.indexed
				}
				if (indexed !== t.item.indexed) {
					t.item.indexed = indexed;
					t.setIndexedStyle();
					t.dirty();
				}
			},

			setIndexedStyle: function () {
				var t = this;
				t.$el.attr('indexed', t.item.indexed);
			},

			getStandaloneBody: function (content) {
				return utils.getStandaloneBody(content);
			},

			// __m14: method - enhanceEmbeddeds()
			enhanceEmbeddeds: function ($tmp) {
				utils.enhanceEmbeddeds($tmp || this.$('> .item'));
			},

			// __logic: __m21: item.toggleEditMode
			toggleEditMode: function (isEditMode, skipSave) {
				var t = this;
				t.$('> .item').toggleClass('edit-mode', isEditMode);

				if (!isEditMode) {
					if (skipSave && !t.item.content && !t.item.permanent) {
						if (t.$('> .children > li').length) {
							var children = [];
							t.$('> .children > li').each(function () {
								children.push($(this).data('view'));
							});
							t.trigger('move-parent', children);
						}
						t.remove();
					}
				} else {
					t.$('> .item textarea').val(t.item.content);
				}
				t.hide$tagInput(true);
				t.$('> .item textarea').blur();

			},

			// __logic: item.onInsertChild
			onInsertChild: function (e) {
				var t = this;
				// __event: item.new-item-child
				t.trigger('new-item-child');
			},

			// __m8: __m18: scrollInTo view
			scrollIntoView: function (keepLazyHovering, opts) {
				var t = this;
				var $i = t.$('> .item');

				var $body = $('body');
				opts = opts || {};

				if (opts.preview || opts.nextUnread || opts.permaLink) {
					t.trigger('preview-expand');
				} else {
					t.trigger('preview-collapse');
				}

				opts = opts || {};
				var wtop = $(window).scrollTop(), wh = $(window).height(),
					itop = $i.offset().top, ih = $i.outerHeight(true),
					marginBottom = 50, marginTop = 320;
				if (itop + ih > wtop + wh / 1.5) {
					wtop = itop + ih - wh / 1.5;
					$(window).scrollTop(wtop);
				}

				if (itop < wtop + marginTop) {
					$(window).scrollTop(itop - marginTop);
				}

				if (!keepLazyHovering) t.g.stopLazyHovering();
			},

			// __logic: item.onEdit
			onEdit: function (e) {
				var t = this;
				if (!t.$('> .item .edit').length) {
					t.$('> .item').append(t.g.tpls.itemForm());
				}
				t.toggleEditMode(true);
				var itemText = t.$('> .item .edit textarea').focus().get(0);

				var end = 99999;
				if (itemText.setSelectionRange) {
					setTimeout(function () {
						itemText.setSelectionRange(end, end, 0);
					}, 0)
				} else { // IE style
					var aRange = itemText.createTextRange();
					aRange.collapse(true);
					aRange.moveEnd('character', end);
					aRange.moveStart('character', end);
					aRange.select();
				}

				var con = t.item.content, height = 0;
				var lines;

				if (con) {
					lines = con.split(/(\r\n|\n)/);

					var maxHeight = 380;
					var lineCount = 0;

					lines.forEach(function (line) {
						lineCount += Math.ceil(line.length / 55);
					});
					var height = lineCount * 18 + 50;
					if (height > maxHeight) height = maxHeight;
				}
				var $textarea = t.$('> .item textarea');
				$textarea.height(height || 80);

				if (lines && lines.length > 1) {
					tabIndent.render($textarea[0]);
					$textarea[0].blur();
					$textarea[0].focus();
					$textarea.data('tab-inited', true);
				}
				else {
					tabIndent.removeListener($textarea[0]);
					$textarea.data('tab-inited', false);
				}

				// __event: item.item-hover
				t.trigger('item-hover', true);
				e && e.preventDefault();

				t.g.setCurrentEditor(t);
			},

			// __m21: remove tag
			removeTagAtPos: function ($text) {
				var t = this, tagAtPos = t.getTagAtPos($text);

				if (tagAtPos.preM) {
					var preMatch = tagAtPos.preM[1],
						postMatch = tagAtPos.postM ? tagAtPos.postM[0] : '';
					var preText = tagAtPos.preText;
					preText = preText.substr(0, preText.length - preMatch.length - 1);
					var postText = tagAtPos.postText.substr(postMatch.length);
					var offset = 0;
					if (postText.substr(0, 1) == ' ') {
						postText = postText.substr(1);
						offset -= 1;
					}
					$text.val(preText + postText);

					var text = $text.get(0);
					text.selectionEnd = text.selectionStart =
						tagAtPos.pos - preMatch.length + offset;
				}
			},

			tagChosen: function (tagText) {
				var t = this;
				tagText = $.trim(tagText);

				// __m24: logic: put to tagOrder
				var tagOrder = utils.getItem('tag-order');
				if (!tagOrder || typeof tagOrder != 'object') {
					tagOrder = {};
				}

				if (JSON.stringify(tagOrder).length > 100000) {
					var tags = [];
					_.each(tagOrder, function (val, key) {
						tags.push({text: key, time: val});
					});
					tags.sort(function (a, b) {
						if (a.time > b.time) return 1;
						else if (a.time < b.time) return -1;
						else return 0;
					});
					tagOrder = {};
					for (var i = tags.length, l = 0; i-- > 0 && l++ < 100;) {
						tagOrder[tags[i].text] = tags[i].time;
					}
				}

				tagOrder[tagText] = new Date().getTime();

				_.extend(tagOrder, t._tagsOrder);
//                    console.log(tagOrder);
				utils.setItem('tag-order', tagOrder);
				t.g.list.sortTags();
			},

			// __m21: method: select tag
			selectTag: function ($text, tagText) {
				var t = this;
				var text = $text.get(0);
				var tagAtPos = t.getTagAtPos($text);

				if (!tagText) {
					if (t.$tagInput.find('[tag-text]').length) {
						tagText = t.$tagInput.find('[tag-text]').attr('tag-text');
					} else {
						tagText = tagAtPos.tagTerm;
					}
				}

				if (tagAtPos.preM) {
					var preMatch = tagAtPos.preM[1],
						postMatch = tagAtPos.postM ? tagAtPos.postM[0] : '';
					var preLen = tagAtPos.preText.length;
					var preText = tagAtPos.preText.substr(0, preLen - preMatch.length);
					var postText = tagAtPos.postText.substr(postMatch.length);
					if (tagText.substr(tagText.length - 1) != ' ') tagText = tagText + ' ';
					var newValue = preText + tagText + postText;
					$text.val(newValue);

					text.selectionStart = text.selectionEnd = tagAtPos.pos - preMatch.length +
						tagText.length;

					t.tagChosen('#' + tagText);
				}
			},

			// __logic: item.onTextareaKeydown
			onTextareaKeydown: function (e) {
				var t = this, $text = $(e.currentTarget);

				if (e.keyCode == keymap.enter) {
					if (t.$tagInput.is(':visible')
						&& !(e.altKey || e.shiftKey || e.ctrlKey || e.metaKey)) {
						t.selectTag($text);
						t.hide$tagInput();
						e.stopImmediatePropagation();
						e.preventDefault();
						return;
					}

					var currContent = $text.val(), oldContent = t.item.content;
					if (e.altKey || (
						(oldContent && oldContent.indexOf('\n') != -1 ) || currContent.indexOf('\n') != -1)
						&& !(e.shiftKey || e.ctrlKey || e.metaKey)
					) {
						if (e.altKey) {
							quickHelp.incrementCount('multi-line-text');
						}

						e.stopImmediatePropagation();
						e.preventDefault();
						var h = $text.height();
						if (h < 380) {
							$text.height(h + 18);
						}

						var text = $text.get(0), start = text.selectionStart, end = text.selectionEnd;
						var val = $text.val();
						$text.val(val.substr(0, start) + '\n' + val.substr(end));
						text.selectionStart = text.selectionEnd = start + 1;

						if (!$text.data('tab-inited')) {
							tabIndent.render($text[0]);
							$text[0].blur();
							$text[0].focus();
							$text.data('tab-inited', true);
						}

					} else {
						if (e.metaKey) {
							quickHelp.incrementCount('save-edit');
						}
						t.onSaveClick();
						e && e.preventDefault();
					}
				} else if (e.keyCode == keymap.escape) {
					quickHelp.incrementCount('cancel-edit');
					t.onCancelClick();
				} else if (e.keyCode == keymap.e) {
					if (e.ctrlKey || e.metaKey) {
						var text = $text.get(0), start = text.selectionStart, end = text.selectionEnd;

						var val = $text.val();

						var leftPart = val.slice(0, start);
						var rightPart = val.slice(end);

						var leftIndex = leftPart.lastIndexOf('_(');
						var rightIndex = rightPart.indexOf(')_');

						var leftRightIndex = leftPart.lastIndexOf(')_');
						var rightLeftIndex = rightPart.indexOf('_(');

						if (leftIndex !== -1 && rightIndex !== -1 && leftRightIndex < leftIndex
							&& (rightLeftIndex !== -1 && rightLeftIndex > rightIndex)
						) {
							leftPart = leftPart.substr(0, leftIndex) + leftPart.substr(leftIndex + 2);
							rightPart = rightPart.substr(0, rightIndex) + rightPart.substr(rightIndex + 2);

							val = leftPart + val.substring(start, end) + rightPart;
							$text.val(val);

							setTimeout(function () {
								text.selectionStart = start - 2;
								text.selectionEnd = end - 2;
							});
						}
						else {
							if (end - start > 0) {
								$text.val(val.substr(0, start) + '_(' + val.slice(start, end) + ')_' + val.substr(end));

								setTimeout(function () {
									text.selectionStart = start + 2;
									text.selectionEnd = end + 2;
								})
							}
						}

						e.preventDefault();
					}
				}
			},

			getSelections: function () {
				var t = this, $text = t.$('.edit textarea'),
					text = $text.get(0);

				var start = text.selectionStart, end = text.selectionEnd;
				var val = $text.val(), preText = val.substr(0, start), postText = val.substr(end),
					middleText = val.substr(start, end - start);

				return {
					$text: $text,
					text: text,
					start: start,
					end: end,
					middleText: middleText,
					preText: preText,
					postText: postText
				}
			},

			rgxIncludesAnyTag: /(^|\s|\t|\n)#/,
			tryConvertTags: function (e) {
				var t = this;

				var sel = t.getSelections();

				if (sel.start != sel.end) {
					if (!t.rgxIncludesAnyTag.exec(sel.middleText)) {
						sel.middleText = sel.middleText ? sel.middleText : '';
						sel.$text.val(sel.preText + ' #' + sel.middleText.split(/[\n\s\t#]+/).join('-') + ' ' + sel.postText);
						sel.text.selectionEnd = sel.end + 3;
					}
					e.preventDefault();
				}
			},

			onTextareaTag: function (e) {
				var t = this, $text = $(e.currentTarget);
				t.throttleTags($text, t);
			},

			// __m21: method: hoverOnTag
			hoverOnTag: function (e) {
				var t = this, $tag = $(e.currentTarget);

				// delay show the input
				_.cancelDelayRun('leave-tag');

				_.delayRun('hover-on-tag', function () {
					var $item = $tag.closest('.item')
					$item.find('.body [tag-text]').removeClass('tag-hovered');
					$tag.addClass('tag-hovered');
					t.$tagInput.addClass('hovered-on-tag');
					$item.append(t.$tagInput);

					var pos = $tag.position();
					pos.top += 21;
					t.$tagInput.css(pos)
						.data('for-tag', true);
//                        console.log('hover hit -');
					t.g.list.filterTagsByGroup($tag.attr('tag-text'));
				}, 300);
			},

			// __m21: method: leaveTag
			leaveTag: function (e) {
				var t = this, $tag = $(e.currentTarget);
				// cancel delay, hide tag input
				_.cancelDelayRun('hover-on-tag');

				_.delayRun('leave-tag', function () {
//                        console.log('leave hit -');
					t.hide$tagInput(true);
				}, 200);
			},

			hoverOnTagInput: function (e) {
				_.cancelDelayRun('leave-tag');
			},

			leaveTagInput: function (e) {
				var t = this;

				_.cancelDelayRun('hover-on-tag');

				if (t.$tagInput.data('for-tag')) {
					_.delayRun('leave-tag', function () {
//                        console.log('leave hit -');
						t.hide$tagInput(true);
					}, 200);
				}
			},

			// __m21: method: cancelHoveringOnTag
			cancelHoveringOnTag: function (e) {
				e.stopImmediatePropagation();
			},

			_tagsOrder: {
				'#task.todo:r2': 200,
				'#task.in-progress:b2': 190,
				'#task.done:g2': 180,
				'#task.pending:br2': 175,
				'#task.canceled:gr2': 170,
				'#question.open:r2': 160,
				'#question.closed:g2': 150,
				'#question.canceled:gr2': 140
			},

			toggleSpecificTag: function (tagText, rgx) {
				var t = this, content = t.item.content;
				if (rgx.exec(content)) {
					var c = content.replace(rgx, '$1');
				} else {
					c = content.replace(/(^h\d+\.|#[^:]+:[\S]*\s*=)*/, '$1 ' + tagText + ' ');
				}

				t.item.content = $.trim(c);
				t.hide$tagInput();
				t.refreshBody();
				t.dirty();
			},

			rgxQuestionOrTaskTag: /((?:^|\s|\n|\t)*)\s?#(question|task)\.\S+\:\!?\S+($|\s|\n|\t)/g,
			toggleQuestionTag: function () {
				this.toggleSpecificTag('#question.open:r2', this.rgxQuestionOrTaskTag);
			},

			toggleTaskTag: function () {
				this.toggleSpecificTag('#task.todo:r2', this.rgxQuestionOrTaskTag);
			},

			// __m21: method: toggleTagTo
			toggleTagTo: function (targetTag, index, tagText) {
				var t = this, item = t.item, content = item.content;
				targetTag = '#' + targetTag;
				tagText = '#' + tagText;

				index++;

				var prts = content.split(targetTag);
				var newContent = [];
				_.each(prts, function (prt, idx) {
					if (!idx) {
						newContent.push(prt);
					} else if (idx == index) {
						if (tagText == '#') {
							if (prt.substr(0, 1) == ' ') prt = prt.substr(1);
							newContent.push('', prt);
						} else {
							newContent.push(tagText, prt);
						}
					} else {
						newContent.push(targetTag, prt);
					}
				});

				t.tagChosen(tagText);

				item.content = newContent.join('');
				t.hide$tagInput();
				t.refreshBody();
				t.dirty();
			},

			// __m21: hide $tagInput
			hide$tagInput: function (globally) {
				var t = this;
				if (!globally) {
					if (!t.$('#tag-input').length) return;
				}
				t.$tagInput
					.removeClass('hovered-on-tag')
					.appendTo(t.$hiddenZone)
					.css({top: 'auto', left: 'auto'})
					.data('for-tag', false);
			},

			_rgxTagPrefix: /(?:^|\s|\t|\n|\r)#([^#\s]*)$/,
			_rgxTagSuffix: /^\S+ ?/,
			getTagAtPos: function ($text) {
				var text = $text.get(0);
				var value = $text.val();
				var t = this;

				var start = text.selectionStart, end = text.selectionEnd;
				if (start == end) {
					// __m21: getting values after #
					var preText = value.substring(0, end), postText = value.substring(end);
					var preM = t._rgxTagPrefix.exec(preText),
						postM = t._rgxTagSuffix.exec(postText);
				}

				if (preM) {
					var postTerm = (postM ? postM[0] : '');
					var tagTerm = preM[1] + postTerm;

					return {
						preText: preText,
						postText: postText,
						preM: preM,
						postM: postM,
						postTerm: postTerm,
						preTerm: preM[1],
						tagTerm: tagTerm,
						pos: start
					};
				} else {
					return {};
				}
			},

			// __m29: __m21: method: throttleTags
			throttleTags: _.throttle(function ($text, t) {
				if (!$text.is(':visible')) return;

				var tagAsPos = t.getTagAtPos($text);
				if (tagAsPos.preM) {
					var pret = tagAsPos.preTerm;
					if (pret.indexOf(';') != -1) {
						var prt = pret.split(';');
						tagAsPos.preTerm = prt[0];
						var command = prt[prt.length - 1];
						if (command) {
							// __m29: check the command
							var $tag = t.$tagInput.find('[data-shortcut=' + command + ']:visible');
							$tag.click();
							return;
						}
					}

					console.log('# text - ', tagAsPos.tagTerm);
					// display the tag input

					var $item = $text.closest('.item');
					t.$tagInput.appendTo($item);

					t.$tagInput.find('.filter input').val(tagAsPos.preTerm);
					t.g.list.searchTags(tagAsPos.preTerm);
				} else {
					t.hide$tagInput();
				}
			}, 800, {trailing: true}),

			// __m14: method - onTextareaPaste()
			onTextareaPaste: function (e) {
				var t = this;
				var items = (e.clipboardData || e.originalEvent.clipboardData).items,
					imageUrls = {}, $text = $(e.currentTarget);
				if (!items) {
					noty({
						text: 'This browser doesn\'t support pasting image directly from clipboard at the moment. Please use <b>Chrome</b>',
						type: 'warning',
						layout: 'bottomRight',
						timeout: 1000
					})
					return;
				}

				if (items && items.length) {
					var html = e.originalEvent.clipboardData.getData('text/html');
					var rgxImages = /src="(.*?)"/ig, rgxHref = /href="(.*?)"/ig;

					do {
						var match = rgxImages.exec(html);
						if (!match) break;
						else imageUrls[match[1]] = 1;
					} while (true);

					_.some(items, function (item) {
						if (item.type.substr(0, 6) == 'image/') {
							var type = item.type.substr(6);
							var blob = item.getAsFile();
							var reader = new FileReader();
							t.g.app.blockScreen(true);

							reader.onload = function (event) {
								ajax.post('saveImage', {
									image: event.target.result,
									type: type
								}, {
									ok: function (url) {
										var pos = $text.get(0).selectionStart;
										var val = $text.val();
										val = val.substr(0, pos) + ' ![image](' + url + ') ' + val.substr(pos);
										$text.val(val);
										$text.get(0).selectionStart = pos;
										$text.get(0).selectionEnd = pos;
									},
									invalidType: function () {
										// should not reach here, unless someone try to hack the system
									},
									anyError: function () {
										noty({
											text: 'Some error occurs, image not saved.',
											layout: 'bottomRight',
											type: 'warning',
											timeout: 2000
										});
									},
									last: function () {
										t.g.app.blockScreen(false);
									}
								})
								return;
							}; // data url!
							reader.readAsDataURL(blob);
							return true;
						}
					});

					setTimeout(function () {
						var val = $text.val();
						_.each(imageUrls, function (_, image) {
							val += '\n![image](' + image + ')';
						});

						$text.val(val);
					})
				}
			},

			// __logic: __m21: item.onSaveClick
			onSaveClick: function (e) {
				var t = this;

				this.save();
				t.toggleEditMode(false);
				e && e.stopPropagation();
				this.refreshBody();
				t.postSaveActions();

				t.g.clearCurrentEditor();
			},

			postSaveActions: function () {
				var t = this;
				t.g.list.detectDef(t.item.def, t);
				t.g.list.detectDict(t.item);
			},

			// __logic: __m21: item.onCancelClick
			onCancelClick: function (e) {
				var t = this;
				t.g.clearCurrentEditor();
				t.toggleEditMode(false, true);
				e && e.stopPropagation();

			},

			onToggleSelect: function () {
				this.toggleSelect();
			},

			// __logic: onToggleSelect
			toggleSelect: function (enabled) {
				if (typeof enabled == 'object') {
					var $t = $(enabled.currentTarget);
					if ($t.is('.edit-mode')) return;
				}
				return this.g.toggleSelect(this, enabled);
			},


			focusOnItem: function () {
				var t = this, $con = t.g.list.$content;

				if ($con.is('.focus-mode')) {
					t.g.list.trigger('focus-cancelled');
					$con.removeClass('focus-mode');
					$('li.focus').removeClass('focus');
				} else {
					t.trigger('item-focused');
					$con.addClass('focus-mode');
					t.$el.addClass('focus');
				}
			},

			// __logic: __m11: __m21: method: item.onToggleTaskStatus
			onToggleTaskStatus: function () {
				var t = this;
				var i = t.item;

				var $item = t.$('> .item'),
					$hovered = $item.find('.is-task.tag-hovered'),
					$first = $item.find('.is-task').eq(0);

				var toggleTag = function ($tag) {
					var tagText = $tag.attr('tag-text'),
						$same = $item.find('> .body [tag-text="' + tagText + '"]');
					var nextTag;

					if (tagText == 'task.todo:r2') {
						nextTag = 'task.done:g2';
						utils.taskDone(t, t.g.app);
					} else if (tagText == 'task.done:g2') {
						nextTag = 'task.canceled:gr';
						utils.taskDone(t, t.g.app);
					} else if (tagText == 'question.open:r2') {
						nextTag = 'question.closed:g2';
					} else if (tagText == 'question.closed:g2') {
						nextTag = 'question.canceled:gr2';
					} else if (tagText.substr(0, 5) == 'task.') {
						nextTag = 'task.todo:r2';
					} else if (tagText.substr(0, 9) == 'question.') {
						nextTag = 'question.open:r2';
					}

					if (nextTag) {
						t.toggleTagTo(tagText, $same.index($tag), nextTag);
					}
				}

				if ($hovered.length) {
					toggleTag($hovered);
				} else if ($first.length) {
					toggleTag($first);
				} else {
					i.content = i.content.replace(/(^(?:h\d+\.|#[^:]+:[\S]*\s*=)?)/, '$1 #task.todo:r2 ');
				}

				t.hide$tagInput();
				t.refreshBody();
				t.dirty();
			},


			resetOld: function (patch) {
				var t = this;
				if (patch) {
					_.extend(t.old, patch);
				} else {
					t.old = _.extend({}, t.item);
				}
				this.item.forceSave = false;
			},

			touchItem: function () {
				this.item.tempUpdated = new Date().getTime();
			},

			// __logic: item.getPrevItem
			getPrevItem: function () {
				var t = this;
				var $prev = t.$el.prev(':visible');
				if ($prev.length) {
					return t.getLastMostItem($prev);
				}

				$prev = t.$el.parents('li:visible').eq(0);
				if ($prev.length) return $prev;

				return false;
			},

			getLastMostItem: function ($item) {
				var t = this, $children = $item.find('> .children > li:visible');
				if ($children.length) {
					$item = $children.eq(-1);
					return t.getLastMostItem($item);
				}
				return $item;
			},

			// __logic: item.getNextItem
			getNextItem: function () {
				var t = this;

				$next = t.$('> .children > li:visible').eq(0);
				if ($next.length) return $next;

				var $next = t.$el.next(':visible');
				if ($next.length) return $next;

				return t.getFirstMostItem(t.$el);

			},

			getFirstMostItem: function ($item) {
				var t = this, $item = $item.parents('li:visible').eq(0);

				if ($item.length) {
					$next = $item.next(':visible');
					if ($next.length) return $next;
					else return t.getFirstMostItem($item);
				}
				return false;
			},

			// __logic: item.getParent
			getParent: function () {
				var t = this;
				var $parent = t.$el.parents('li:visible').eq(0);
				if ($parent.length) return $parent;
				return false;
			},

			// __logic: item.rememberChild
			rememberChild: function () {
				var t = this;
				var $parent = t.getParent();
				if ($parent) {
					var parent = $parent.data('view');
					parent._recallChild = t;
				}
			},

			// __logic: item.recallChild
			_recallChild: null,
			recallChild: function () {
				var t = this;
				if (t._recallChild) {
					t._recallChild.$el;
					if (t._recallChild.$el.is(':visible')) return t._recallChild.$el;
					return false;
				} else {
					$child = t.$('> .children > li:visible');
					if ($child.length) return $child.eq(0);
					return false;
				}
			},


			// __m11: __m19: method - item.setTaskPoint()
			setTaskPoints: function (points) {
				var t = this;

				var updateTaskPoints = function (points) {
					t.item.task_points = points;
//                if (points == 0) {
//                    t.toggleType(false);
//                } else {
//                    t.toggleType(true);
//                }
				}

				if (typeof points == 'number') {
					updateTaskPoints(points + t.item.taskSpent);
				} else if (points == '=') {
					var userInput = prompt('Please assign the task points for this item',
						utils.formatDuration(t.item.task_points * 10));
					var points = utils.fromDurationText(userInput) / 600000;
					updateTaskPoints(points + t.item.taskSpent);
				}
			},

			// __logic: item.checkChildren
			checkChildren: function (hasChildren) {
				var t = this;

				hasChildren = typeof hasChildren == 'boolean' ? hasChildren :
					t.$('> .children > li').length;
				t.$el.toggleClass('no-child', !hasChildren);
			},

			// __logic: item.setSelect
			setSelect: function (selected) {
				var t = this;
				t.$el.toggleClass('selected', selected);
			},

			// __logic: item.dirty
			dirty: function () {
				var t = this;
				t.isDirty = true;
				t.item.isNew = false;
				t.trigger('item-dirty');
				t.item.created_by = t.item.created_by || t.g.userId;

				if (t.item.content != t.old.content || t.item.forceSave) {
					console.log('touch item - ', t.item.id);
					t.item.updated_by = t.g.userId;
					t.touchItem();
				}
			},

			// if force save is set to be true, we save it anyway
			// __logic: item.save
			save: function () {
				var t = this;
				var $text = t.$('> .item textarea');

				t.item.isNew = false;
				var content = $text.val();
				t.item.content = utils.addColonForTags(content);
				t.g.list.detectItemTags(t, t.item.content);

				this.trigger('item-saved');

				t.dirty();
			},

			shareItem: function (e) {
				var t = this;

				var listTitle = t.g.list.list.title;
				var c = t.$el.find('> .item .body').text();

				prompt('节点分享内容',
					'分享内容 : ' + c + '\n\n' +
					'来自列表 : ' + listTitle + '\n\n' +
					location.origin + '/app/list/' + t.g.list.list.id + '/' + t.item.id
				);
			},

			removeItem: function (e) {
				this.remove();
				e.stopImmediatePropagation();
			},

			// __logic: __m11: item.remove
			remove: function (isUIRemoveOnly) {
				var t = this;
				t.hide$tagInput()

				var $parent = t.getParent();
				if ($parent) {
					var parent = $parent.data('view');
					if (parent._recallChild == t) parent._recallChild = null;
				}

				// __event: item.remove-item
				t.trigger('remove-item', isUIRemoveOnly);

				t.g = null;
			},

			// __logic: item.getId
			getId: function () {
				var t = this;
				var rand = Math.floor(Math.random() * t.g.bigint).toString(36);
				return rand + '_'
					+ (t.g.idSeq++).toString(36)
					+ t.g.userSeq;
			},

			// __m8: isExpanded
			isExpanded: function () {
				var t = this;
				return !t.$el.is('.is-collapsed');
			}

		});


		Item.initStatic = function (g) {
			var emptyItemView = new Item({g: g});
			Item.getStandaloneBody = _.bind(emptyItemView.getStandaloneBody, emptyItemView);
		}

		return Item;

	});
