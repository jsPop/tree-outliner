define(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5',
		'syncer', 'header-dropdown', 'aui-dropdown2',
		'spinner', 'window-messenger', 'item-processor', 'marked', 'pickmeup',
		'moment', 'chinese-pinyin'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5,
			  Syncer, HeaderDropdown, _dropdown2,
			  _spinner, messenger, itemProcessor, marked, pickmeup,
			  moment, chinesePinyin,
			  _undefined) {

		var ignoreKey = navigator.userAgent.indexOf('Macintosh') !== -1 ? 'ctrl+' : 'cmd+';

		var helpMap = {};
		helpContent.forEach(function (section) {
			section.items.forEach(function (item) {
				var chars = item.title.split('').map(function (char) {
					if (chinesePinyin._[char]) {
						console.warn(char, '存在多音字');
					}
					return chinesePinyin[char];
				});

				item.pinyin = chars.join('');
				item.pinyinWithSpace = chars.join(' ');

				item.shortcut = item.shortcut.filter(function (shortcut) {
					return shortcut.indexOf(ignoreKey) === -1;
				});

				if (helpMap[item.key]) {
					console.warn(item.key, 'help key 重复了');
				}
				helpMap[item.key] = item;
			});
		});


		var LOCAL_KEY = 'outliner_help_config';
		var LOCAL_TIME_KEY = 'outliner_help_config_timestamp';

		var quickHelpUtils = {
			init: function (g) {
				var t = this;

				t.$panel = $('#help-panel');

				t.initCloseEvent();

				if (window.top !== window) {
					return;
				}

				t.$trigger = $('#help-trigger');
				t.$detail = $('#help-detail');
				t.$content = $('#help-content tbody');
				t.$contentOuter = $('#help-content-outer');

				t.g = g;

				t.$panel.show();
				t.generateRandomHelp();

				t.setVisibility();
				t.resetHelpContentCount();
				t.refreshContent();

				t.initEvents();

				t.initRandomSuggest();

				window.openHelp = t.openHelp.bind(t);
				window.incrementHelpCount = t.incrementCount.bind(t);
				window.setHelpVisibility = t.setVisibility.bind(t);
			},

			initRandomSuggest: function () {
				var t = this;

				var refreshRandomSuggest = function () {
					if (t.$trigger.find('input').is(':focus')) {
						return;
					}

					t.generateRandomHelp();
					t.debounceRefreshContent();
				};

				setInterval(refreshRandomSuggest, 5 * 60 * 1000);
			},

			initCloseEvent: function () {
				var t = this;

				$(document).on('click', function () {
					t.setVisibility(false);
				});
			},

			initEvents: function () {
				var t = this;

				t.$trigger.on('keyup', 'input', function (e) {
					t.debounceRefreshContent(e.target.value);
				}).on('keydown', 'input', function(e) {
					if (e.keyCode == keymap.escape) {
						e.target.value = '';
						t.setVisibility(false);
						$(this).blur();
					}
				}).on('focus', 'input', function(e) {
					t.setVisibility(true);
					t.debounceRefreshContent(e.target.value);
				}).on('click', '.trigger', function (e) {

				}).on('click', function (e) {
					e.stopPropagation();
				});

				t.$contentOuter.on('click', function (e) {
					e.stopPropagation();
				}).on('mouseenter', 'tr.help-item', function () {
					var $t = $(this);
					var helpKey = $t.attr('data-key');

					t.showDetail(helpKey);
				}).on('mouseleave', function () {
					t.debounceHideDetail();
				}).on('mouseenter', function () {
					t.debounceHideDetail.cancel();
				});
			},

			showDetail: function (helpKey) {
				var t = this;

				var item = helpMap[helpKey];
				if (!item) {
					t.$detail.hide();
					return;
				}

				if (helpKey !== t.currentHelpKey) {
					var html = t.g.tpls.quickHelpDetail({
						title: item.title,
						shortcut: item.shortcut,
						count: item.count,
						detailHtml: item.detailHtml
					});
					t.$detail.find('.content').html(html);
					t.currentHelpKey = helpKey;
				}

				t.debounceHideDetail.cancel();
				t.$detail.show();
			},

			hideDetail: function () {
				this.debounceHideDetail.cancel();
				this.$detail.hide();
			},

			setVisibility: function (visible) {
				var t = this;

				if (window.top !== window) {
					window.top.setHelpVisibility(visible);
					return;
				}

				if (typeof visible === 'undefined') {
					var helpConfig = t.getHelpConfig();
					visible = new Date().getTime() > helpConfig.nextOpen;
				}
				else if (!visible) {
					var helpConfig = t.getHelpConfig();
					helpConfig.nextOpen = new Date().getTime() + 7 * 24 * 60 * 60 * 1000;
					t.saveHelpConfig();
				}

				t.$contentOuter.toggle(visible);
			},

			generateRandomHelp: function () {
				var t = this;
				var helpItems = [];
				var total = 0;

				// 计算 weight total (用于做随机数值获取), 并生成扁平的 items 列表
				helpContent.forEach(function (section) {
					section.items.forEach(function (item) {
						item.count = item.count || 0;

						var weight = ~~item.weight - item.count;
						weight = weight < 1 ? 1 : ~~weight;

						total += weight;
						item.finalWeight = weight;

						helpItems.push(item);
					});
				});

				var randNumbers = [];
				for (var i = 0; i < 6; i++) {
					randNumbers.push(Math.floor(Math.random() * total));
				}

				randNumbers.sort(function (a, b) {
					return a > b ? 1 : a < b ? -1 : 0;
				});

				var currentRandNumber = randNumbers.shift();
				var currentWeightIndex = 0;
				var randItems = helpItems.filter(function (item) {
					var found = false;
					var nextWeightIndex = currentWeightIndex + item.finalWeight;
					if (currentRandNumber >= currentWeightIndex && currentRandNumber < nextWeightIndex) {
						currentRandNumber = randNumbers.shift();
						found = true;
					}

					while (nextWeightIndex > currentRandNumber && randNumbers.length) {
						currentRandNumber = randNumbers.shift();
					}

					currentWeightIndex = nextWeightIndex;

					return found;
				});

				t.randItems = randItems;
				return randItems;
			},

			timestamp: -1,
			getHelpConfig: function () {
				var t = this;

				var timestamp = parseInt(localStorage.getItem(LOCAL_TIME_KEY));
				timestamp = isNaN(timestamp) ? 0 : timestamp;
				if (t.timestamp < timestamp) {
					t.helpConfig = utils.getItem(LOCAL_KEY, {
						nextOpen: 0,
						helpItems: {}
					});

					t.helpConfig.nextOpen = parseInt(t.helpConfig.nextOpen);
					if (isNaN(t.helpConfig.nextOpen)) {
						t.helpConfig.nextOpen = 0;
					}

					t.resetHelpContentCount();

					t.timestamp = timestamp
				}

				return t.helpConfig;
			},

			saveHelpConfig: function () {
				var t = this;

				t.timestamp = new Date().getTime();
				localStorage.setItem(LOCAL_TIME_KEY, t.timestamp);
				utils.setItem(LOCAL_KEY, t.helpConfig);
			},

			incrementCount: function (helpKey) {
				var t = this;

				if (window.top !== window) {
					window.top.incrementHelpCount(helpKey);
					return;
				}

				var config = t.getHelpConfig();

				config.helpItems = config.helpItems || {};
				var itemConfig = config.helpItems[helpKey] = config.helpItems[helpKey] || {count: 0};

				if (itemConfig > 999) return;

				itemConfig.count ++;
				if (isNaN(itemConfig.count)) {
					itemConfig.count = 1;
				}

				t.saveHelpConfig();
				t.resetHelpContentCount(helpKey, itemConfig);
				t.debounceRefreshContent();
			},

			openHelp: function() {
				var t = this;

				if (t.$trigger) {
					t.$trigger.find('input').focus();
				}
				else {
					window.top.openHelp();
				}
			},

			isResetingHelpContentCount: false,
			resetHelpContentCount: function (helpKey, itemConfig) {
				var t = this;
				if (t.isResetingHelpContentCount) {
					return;
				}
				t.isResetingHelpContentCount = true;

				if (helpKey) {
					var itemContent = helpMap[helpKey] || {count: 0};

					itemContent.count = itemConfig.count;
				}
				else {
					var helpConfig = t.getHelpConfig();
					_.each(helpMap, function(item, helpKey) {
						var itemConfig = helpConfig.helpItems[helpKey];

						item.count = itemConfig && itemConfig.count || 0;
					});
				}

				t.isResetingHelpContentCount = false;
			},

			refreshContent: function (searchTerm) {
				var t = this;

				searchTerm = searchTerm || '';
				searchTerm = _.trim(searchTerm);

				t.debounceRefreshContent.cancel();

				var contentData = [];

				if (!searchTerm) {
					contentData.push({
						title: '随机推荐',
						items: t.randItems.map(function (item) {
							return _.pick(item, ['key', 'title', 'shortcut', 'count']);
						})
					});
				}

				helpContent.forEach(function (section) {
					var items = section.items.filter(function (item) {
						return item.title.indexOf(searchTerm) !== -1
							|| item.pinyin.indexOf(searchTerm) !== -1
							|| item.pinyinWithSpace.indexOf(searchTerm) !== -1;
					}).map(function (item) {
						return _.pick(item, ['key', 'title', 'shortcut', 'count'])
					});

					if (items.length) {
						contentData.push({
							title: section.title,
							items: items
						});
					}
				});

				var html = t.g.tpls.quickHelp(contentData);
				t.$content.html(html);
			}
		};

		_.extend(quickHelpUtils, {
			debounceRefreshContent: _.debounce(quickHelpUtils.refreshContent, 220),
			debounceHideDetail: _.debounce(quickHelpUtils.hideDetail, 250)
		});

		return quickHelpUtils;
	});
