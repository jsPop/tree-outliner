define(['utils', 'underscore', 'jquery'],
	function (utils, _, $) {

		function calculate() {
			var $m = $('#main-list');
			var rgxTaskTag = /#task\.(\w+):/;
			var rgxTaskInfo = /(?: )?(?:#info\.\d+\/\d+[\S]*\s+)?@!\s*(\d+)?(?:\s*:\s*(\d+))?\s*$/;

			function getView($li) {
				return $li.data().view;
			}

			function getTaskStatus(item) {
				var taskMatch = rgxTaskTag.exec(item.content);

				return taskMatch && taskMatch[1];
			}

			function traverseTree($nodeList, preEach, postEach, parentPreEachResult) {
				parentPreEachResult = parentPreEachResult || {};
				var currentPostEachResult = [];

				$nodeList.each(function () {
					var $node = $(this);
					var view = getView($node);

					var preEachResult = preEach(view, parentPreEachResult);
					var childrenPostResult = traverseTree(
						$node.find('> ul > li'),
						preEach,
						postEach,
						preEachResult
					);
					var postEachResult = postEach(view, childrenPostResult, preEachResult);

					currentPostEachResult = currentPostEachResult.concat(postEachResult);
				});
				return currentPostEachResult;
			}

			traverseTree(
				$m.children('li'),
				function (view, parentPreEachResult) {
					var item = view.item;
					var taskStatus = getTaskStatus(item);
					var taskInfoMatch = rgxTaskInfo.exec(item.content);

					var result;
					if (!parentPreEachResult.level) {
						result = {
							id: view.cid
						}
					}
					else {
						result = {
							id: view.cid
						}
					}

					var self = 0, limit = 0, isTask = false;
					if (taskInfoMatch) {
						self = ~~taskInfoMatch[1] || 0;
						limit = ~~taskInfoMatch[2] || 0;
						isTask = true;
					}
					_.extend(
						result,
						{
							isDone: parentPreEachResult.isDone || taskStatus === 'done',
							isCanceled: parentPreEachResult.isCanceled || taskStatus === 'canceled',
							isSelfTodo: taskStatus === 'todo',
							self: self,
							limit: limit,
							isTask: isTask
						}
					);
					return result;
				},
				function (view, childrenPostResult, preEachResult) {
					if (preEachResult.isTask) {
						var existing = 0;
						var total = 0;

						if (!preEachResult.isCanceled) {
							childrenPostResult.forEach(
								function (childPostResult) {
									existing += childPostResult.existing;
									total += childPostResult.total;
								}
							);

							if (!preEachResult.isDone || preEachResult.isSelfTodo) {
								existing += preEachResult.self;
							}
							total += preEachResult.self;
						}

						_.extend(
							preEachResult,
							{
								existing: existing,
								total: total,
								view: view,
								$el: view.$el
							}
						);

						var newContent = view.item.content.replace(
							rgxTaskInfo,
							function () {
								var selfInfo = '' + preEachResult.self;
								if (preEachResult.limit) {
									selfInfo += ':' + preEachResult.limit;
								}

								var upperNodeInfo = '';
								if (childrenPostResult.length) {
									var style = 'b';
									if (existing === 0) {
										style = 'g';
									}
									else if (preEachResult.limit && total > preEachResult.limit) {
										style = 'r';
									}
									upperNodeInfo = [
										'#info.', total - existing, '/', total, ':', style, ' '
									].join('');
								}

								return [
									' ', upperNodeInfo, '@! ', selfInfo
								].join('')
							}
						);
						// console.log(newContent);
						view.item.content = newContent;
						view.refresh();
						view.dirty();

						return [preEachResult];
					}
					else {
						return childrenPostResult;
					}
				}
			);
		}

		return {
			bind: function($dom) {
				$dom.on('click', calculate)
			}
		}
	}
);
