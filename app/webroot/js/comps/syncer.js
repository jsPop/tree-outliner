define([ 'ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
        'backbone', 'mousetrap', 'keymap', 'noty'],
    function(ajax, $, utils, _, commonTplStr,
             Backbone, Mousetrap, keymap, noty
        , _undefined) {


    // __logic: * Syncer
    var Syncer = Backbone.View.extend({
        CON_polling: 45000,
        initialize: function(opts) {
            var t = this;
            _.extend(t, opts);
            t.isDirty = false;

            _.bindAll(t, 'sync');
            var editSync = t.editSync = _.debounce(function() {
                t.lastSyncReq = 0;
                t.editing = false;
                t.sync();
            }, 3000);

            var quickSync = t.quickSync = _.debounce(function() {
                t.lastSyncReq = 0;
                t.editing = false;
                t.sync();
            }, 1800);

            t.debounceSync = function() {
                t.state('dirty');
                t.needSync = true;
                t.editing = true;
                editSync();
            }

            t.debounceQuickSync = function() {
                t.state('dirty');
                t.needSync = true;
                t.editing = true;
                quickSync();
            }

            setInterval(function() {
                t.sync();
            }, t.CON_polling);

            // __logic: global events
            $(window)
                .blur(function() {
                    t.isFocus = false;
                })
                .focus(function() {
                    t.isFocus = true;
                    t.sync();
                });

            $('#logout').click(function() {
                location = '/logout';
            });

            t.$sync = $('#sync');
            t.$sync.on('click', function() {
                t.sync(true, true);
            })
                .on('mouseenter', function() {
                    var $t = $(this);
                    if (!$t.is('is-syncing')) {
                        $t.data('old-text', $t.html());
                        $t.html('立即同步');
                    }
                })
                .on('mouseleave', function() {
                    var $t = $(this);
                    if ($t.data('old-text')) {
                        $t.html($t.data('old-text'));
                    }
                });

            $('#markdown .actions .refresh')
				.click(function() {
					t.sync(true, true);
				});
        },

        state: function(state) {
            var t = this;
            t.$sync.removeClass('is-syncing')
                .data('old-text', false);
            if (state == 'dirty') {
                t.$sync.html('立即同步');
            } else if (state == 'synced') {
                t.$sync.html('已同步');
            } else if (state == 'conflict') {
                t.$sync.html('解决冲突中 ...');
            } else if (state == 'syncing') {
                t.$sync.html('正在同步');
            }

            if (state == 'syncing' || state == 'conflict') {
                t.$sync.addClass('is-syncing');
            }
        },

        needSync: false,
        lastSyncReq: 0,
        lastSynced: 0,
        _isSyncing: false,
        isFocus: true,
        syncingTimer: null,
        previousReq: null,

        applyPatch: function(patch, updated) {
            var t = this;
            if (patch && patch.length) {
                t.list.applyPatch(patch);

                noty({
                    text: '文档有更新, 已加载 ;)',
                    timeout: 2000,
                    layout: 'bottomRight'
                });

				if (t.g.app.withinMarkdownPage) {
					t.g.markdown.refresh();
				}
            }

			t.g.app.updateListVersion(updated);
        },

        updateSuccess: function(itemUpdates) {
            var t = this;
            itemUpdates.forEach(function(update) {
                update.view.resetOld(update.patch);
                delete update.view.item.forceSave;
            });
            t.list.cleanItemIndex();
        },

        sync: function(force, abortPrevious, opts) {
            var t = this;
            var req = {};

            t.list.refreshSavedSearches();

            opts = opts || {};
            if (!t.list) return;

			if (t.g.app.withinSlidesPage || t.g.app.withinPlayPage || t.g.app.withinSlidesV2Page) {
				t.needSync = false;
				return;
			}

            var now = new Date().getTime();
            if (!force
				&& (
					now - t.lastSyncReq < t.CON_polling * 2 / 3
                	|| (t.lastSyncReq != 0 && !t.isFocus)
                	|| t.editing
				)
			) {
            	return;
			}

			t.editSync.cancel();
            t.quickSync.cancel();

            t.lastSyncReq = now;

            if (t._isSyncing && !abortPrevious) return;
            abortPrevious && t.previousReq && t.previousReq.abort();
            t._isSyncing = true;

            var itemUpdates = t.list.getChanges();

            req.patch = _.map(itemUpdates, function(update) {
                return update.patch
            });

            // moving items
            req.children = opts.children;
            req.items = opts.items;
            req.parentItem = opts.parentItem;

            // syncing dicts
            req.dicts = t.list.getDicts(false);
            req.newDicts = t.list.getDicts(true);

            req.itemStatus = t.list.getStatusChanges(force);
            req.listId = t.list.list.id;
            req.lastSynced = parseInt(t.lastSynced);
            req.userHash = t.g.userHash;
            req.today = utils.getToday();
            req.now = new Date().getTime();

            var onError = function() {
                t.state('error');
                t._isSyncing = false;
                reqCall.abort();
            }

            try {
                clearTimeout(t.syncingTimer);
                t.state('syncing');
                var reqCall = t.previousReq = ajax.post(opts.action || 'sync', {
                    req: JSON.stringify(req)
                }, {
                    ok: function(res) {
                        t.applyPatch(res.patch, res.updated);
                        t.updateSuccess(itemUpdates);
                        t.lastSynced = res.synced;
                        t.state('synced');

                        if (t.needSync || (res.patch && res.patch.length)) {
							if (t.g.list.isWithinIframe) {
								window.top.syncer.sync(true);
							}
							else if ($('#page').is('.sub-content-opened') && $('#sub-content')[0].contentWindow.syncer) {
								$('#sub-content')[0].contentWindow.syncer.sync(true);
							}
						}

                        t.needSync = false;
                    },
                    conflict: function(res) {
                        t.applyPatch(res.patch, res.updated);
                        t.lastSynced = res.synced;
                        t.state('conflict');
                        t.debounceQuickSync();
                    },
                    readonly: function(res) {
                    	t.applyPatch(res.patch, res.updated);
                    	t.lastSynced = res.synced;
						t.state('synced');
						t.needSync = false;

						if (res.patch && res.patch.length) {
							noty({
								text: '本文档为只读文档, 只加载文档更新, 不接受修改 ;)',
								timeout: 2000,
								layout: 'bottomRight'
							});
						}
					},
                    anyError: function() {},
                    last: function(res) {
						t._isSyncing = false;
                    	if (!res) {
                    		return;
						}
                        if (res.newDicts) {
                            _.each(req.newDicts, function (listId) {
                                t.list.setDictSynced(listId, res.newDicts[listId]);
                            });
                            t.list.syncDict(res.dicts);
                        }
                    }
                });
                t.syncingTimer = setTimeout(onError, t.CON_polling);
            } catch (ex) {
                t._isSyncing = false;
                onError();
                // __todo: to remove the debugger
                debugger;
            }
            t.list.statusDirty(false);
        }
    });

        return Syncer;
});
