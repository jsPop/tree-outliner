define(['jquery', 'lodash', 'keymap'], function ($, _, keymap) {

	var slidesV2Utils = {
		init: function (g) {
			var t = this;

			t.g = g;
			t.$nodeMap = {};

			t.nodeRel = {'': t.getInitNode()};
			t.nodeList = [];
			t.visited = {};

			t.$page = $('#page');

			t.initNodes();
			t.refresh();

			t.$slidesWrapper = $('#slides-outer .slides-wrapper');
			t.$slidesContent = $('#slides-outer .slides-content');
			t.$aside = $('aside.slides-map');

			t.initEvents();
			t.initUnit();

			t.initHashControl();
		},

		skipHashChange: false,
		initHashControl: function () {
			var t = this;

			var hashChange = function (defaultIndex) {
				if (t.skipHashChange) {
					t.skipHashChange = false;
					return;
				}

				var hash = ~~location.hash.slice(1);
				hash = hash || defaultIndex;
				t.changeIndex(hash);
			};

			$(window).on('hashchange', function() {
				hashChange();
			});

			hashChange(0);
			t.$slidesWrapper.css('opacity', 1);
		},

		getInitNode: function () {
			return {
				id: '',
				children: [],
				parent: null,
				index: 0
			}
		},

		initEvents: function () {
			var t = this;

			$(document).on('keydown', function (e) {
				var kc = e.keyCode;

				if (e.metaKey || e.ctrlKey || e.altKey) {
					return;
				}

				if (kc == keymap.up || kc == keymap.i) {
					t.incrementIndex(-1);
				}
				else if (kc == keymap.down || kc == keymap.space || kc == keymap.k
					|| kc == keymap.right || kc == keymap.l) {
					t.incrementIndex(1);
					e.preventDefault();
				}
				else if (kc == keymap.left || kc == keymap.j) {
					var currentNode = t.nodeList[t.currentIndex];
					if (currentNode.parent.id !== '') {
						t.changeIndex(currentNode.parent.index);
					}
				}
			}).on('click', 'video', function () {
				if (this.paused) {
					this.play();
				}
				else {
					this.pause();
				}
			});

			t.$aside.on('click', '.item', function () {
				var $t = $(this);
				var index = $t.parent().attr('data-index');
				t.changeIndex(index);
			});
		},

		initUnit: function () {
			var t = this;

			var ORIGIN_HEIGHT = 800 + 80;

			var resetUnits = function () {
				var height = $(window).height();
				var width = $(window).width();

				// width / height
				var ratio = 1600 / 900;

				var tryWidth = height * ratio;

				if (tryWidth > width) {
					height = width / ratio;
				}
				else {
					width = tryWidth;
				}

				t.height = height;
				t.width = width;

				var scale = t.height / ORIGIN_HEIGHT
				t.$slidesWrapper.css({
					zoom: scale
				})
			};

			$(window).on('resize', function () {
				resetUnits();
			});
			resetUnits();
		},

		initNodes: function () {
			var t = this;
			var $temp = $('<div/>');

			var traverseNode = function ($nodeList, parentId) {
				$nodeList.each(function () {
					var $t = $(this);

					var v = $t.data('view');
					var item = v.item;

					t.$nodeMap[item.id] = $t.find(' > .item > .body');
					$t.find(' > .item > .body').find('.is-task').remove();
					$t.find('slidestyle').each(function() {
						$(document.head).append($('<style/>').html($(this).html()));
						$(this).remove();
					});
					$t.find('.inner-link')
						.each(function() {
							var $a = $(this);

							$a.attr('href', '/app/list/no-list/' + $a.attr('data-id'))
								.attr('target', '_blank');
						});

					var $itemContentClone = $t.find(' > .item > .body').clone();

					$itemContentClone.find('img').replaceWith('<div>image</div>');
					$itemContentClone.find('video').replaceWith('<div>video</div>');
					$itemContentClone.find('audio').replaceWith('<div>audio</div>');
					$itemContentClone.find('mask').html('?????');

					$temp.append($t);

					var node = t.nodeRel[item.id] = t.getInitNode();
					parentId = parentId || '';
					var parentRel = t.nodeRel[parentId];
					parentRel.children.push(node);
					node.parent = parentRel;

					node.index = t.nodeList.length;
					node.id = item.id;
					node.text = $itemContentClone.text();
					if (!_.trim(node.text)) {
						node.text = 'no text';
					}
					t.nodeList.push(node);

					traverseNode($t.find('> .children > .item-wrapper'), item.id);
				});
			};

			traverseNode($('#main-list > .item-wrapper'));

			console.log(t);
		},

		refresh: function () {
			var t = this;

			t.$page.html(t.g.tpls.slidesV2Wrapper());
			$('body').addClass('slides-v2');
		},

		changeIndex: function (currentIndex) {
			var t = this;
			currentIndex = ~~currentIndex;

			if (currentIndex < 0) {
				currentIndex = 0;
			}
			else if (currentIndex >= t.nodeList.length) {
				currentIndex = t.nodeList.length - 1;
			}

			t.scrollToIndex(currentIndex);
			t.refreshSideList(currentIndex);

			t.currentIndex = currentIndex;

			if (~~location.hash.slice(1) !== t.currentIndex) {
				t.skipHashChange = true;
				location.hash = t.currentIndex;
			}
		},

		incrementIndex: function (delta) {
			var t = this;
			var currentIndex = t.currentIndex + delta;

			t.changeIndex(currentIndex);
		},

		currentIndex: 0,

		addSlide: function (index, skipPrevAndNext) {
			var t = this;

			var node = t.nodeList[index];
			if (!node) return;

			var $slide = $(t.g.tpls.slidesV2Slide({
				index: node.index,
				slideHtml: t.$nodeMap[node.id].html()
			}));

			var prevIndex = index - 1, nextIndex = index + 1;
			var added = false;
			t.$slidesContent.children().each(function () {
				var $currSlide = $(this);
				var currIndex = ~~$currSlide.attr('data-index');

				if (currIndex === nextIndex) {
					nextIndex = -1
				}
				else if (currIndex === prevIndex) {
					prevIndex = -1;
				}

				if (currIndex === index) {
					added = true;
				} else if (!added && currIndex > index) {
					$currSlide.before($slide);

					added = true;
					return true;
				}
			});

			if (!added) {
				t.$slidesContent.append($slide);
			}

			if (!skipPrevAndNext) {
				if (prevIndex != -1) t.addSlide(prevIndex, true);
				if (nextIndex != -1) t.addSlide(nextIndex, true);
			}
		},

		scrollToIndex: function (index, skipAnimaion) {
			var t = this;

			t.addSlide(index);

			var $slide = t.$slidesContent.find('[data-index=' + index + ']');
			var $currSlide = t.$slidesContent.find('[data-index=' + t.currentIndex + ']');

			t.visited[index] = 1;

			if ($currSlide.length) {
				console.log('curr', -1 * $currSlide.position().top);
				t.$slidesContent.addClass('no-animation');
				t.$slidesContent.css({
					top: -1 * $currSlide.position().top
				});

				$currSlide.removeClass('current-slide');
			}

			setTimeout(function () {
				console.log('curr', -1 * $slide.position().top);
				t.$slidesContent.removeClass('no-animation');
				t.$slidesContent.css({
					top: -1 * $slide.position().top
				});

				$slide.addClass('current-slide');
			});
		},

		refreshSideList: function (index) {
			var t = this;

			var targetNode = t.nodeList[index];
			if (!targetNode) return;

			var parents = [targetNode];
			var traverseParents = function (node) {
				if (node.parent) {
					parents.unshift(node.parent);
					traverseParents(node.parent);
				}
			};
			traverseParents(targetNode);

			var $wrapper = t.$aside;
			$wrapper.empty();
			parents.forEach(function (parent, index) {
				if (parent.id) {
					$wrapper = $wrapper.find('[data-index=' + parent.index + '] > .children');
				}

				parent.children.forEach(function (node) {
					$wrapper.append(t.g.tpls.slidesV2ListItem({
						index: node.index,
						text: node.text,
						id: node.id,
						targetId: targetNode.id,
						visited: t.visited
					}));
				});
			});

			var $current = t.$aside.find('[data-index=' + index + ']');
			var top = $current.position().top;
			var scrollTop = t.$aside.scrollTop();
			var height = t.$aside.height();

			if (top < 0) {
				t.$aside.scrollTop(scrollTop + top - 30);
			}
			else if (top > height) {
				t.$aside.scrollTop(scrollTop + top - height + 30);
			}

			console.log(top, scrollTop);
		}
	};

	return slidesV2Utils;
});
