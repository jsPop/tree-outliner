define(['jquery', 'lodash', 'ajax', 'keymap'], function ($, _, ajax, keymap) {
	var channelCache;
	var lastUpdatedMS = -1;
	var updateIntervalMS = 5000;
	var isFirstLoad = true;

	var Channel = Backbone.View.extend({
		/**
		 * Channel
		 *    $el: $container
		 *
		 *    // undefined == ''
		 *    highlightText: string | '' | undefined
		 *
		 *    // 是否展示 "所有" 的筛选项 (review steam 中需要)
		 *    showAll: boolean
		 *
		 *    tpls: {}
		 *        reviewChannelBase: () => string
		 *        reviewChannelGroup: ({ channel }) => string
		 */
		initialize: function (opts) {
			var t = this;
			_.extend(t, opts);

			t.$el.html(t.tpls.reviewChannelBase());

			t.$input = t.$('input');
			t.$result = t.$('.channel-result');
			t.$group = t.$('.channel-group');

			if (!isFirstLoad) {
				t.refreshGroup();
			}

			t.fetchChannels();

			t.initEvent();
		},

		initEvent: function () {
			var t = this;

			t.$el
				.on('keydown', 'input', function (e) {
					e.stopPropagation();
				})
				.on('keyup', 'input', function (e) {
					t.showSearch();
				})
				.on('focus', 'input', function () {
					t.showSearch();
				})
				.on('blur', 'input', function () {
					t.$result.hide();
				})
				.on('mousedown', '.search-result .text', function () {
					var $result = $(this).parent();

					t.addChannel($result);
				})
				.on('mousedown', '.search-result .remove', function () {
					var $result = $(this).parent();

					t.removeChannel($result);
				})
				.on('click', '.remove-displayed', function (e) {
					var $result = $(this).parent();

					t.removeDisplayed($result);
					e.stopPropagation();
				})
				.on('click', '.channel', function () {
					var $result = $(this);

					t.selectChannel($result);
				})
		},

		selectChannel: function ($channelItem) {
			var t = this;
			var text = $channelItem.data('text');

			t.trigger('on-select-channel', {
				channelText: text
			});

			t.highlightText = text;
			t.refreshGroup();
		},

		checkHighlightAfterRemove: function () {
			var t = this;

			var found = channelCache.some(function(channel) {
				return channel.label_text === t.highlightText
					&& channel.is_displayed === 't';
			});

			if (!found) {
				t.highlightText = '';
			}

			t.trigger('on-select-channel', {
				channelText: ''
			});
		},

		removeDisplayed: function ($channelItem) {
			var t = this;
			var text = $channelItem.data('text');

			channelCache.forEach(function (channel) {
				if (channel.label_text === text) {
					channel.is_displayed = 'f';
				}
			});

			t.checkHighlightAfterRemove();

			t.refreshGroup();

			ajax.post('removeChannelDisplayed', {
				channelText: text
			}, {
				ok: function () {
				}
			});
		},

		showSearch: function () {
			var t = this;
			var val = t.$input.val();

			var channels = channelCache.filter(function (channel) {
				return channel.label_text.indexOf(val) !== -1;
			});

			var found = channels.some(function (channel) {
				if (channel.label_text === val) {
					return true;
				}
			});

			if (!found && _.trim(val)) {
				channels.unshift({label_text: val, isNew: true})
			}

			t.$result.html(t.tpls.reviewChannelSearch({
				searchResult: channels
			}));

			t.$result.toggle(!!channels.length);
		},

		removeChannel: function ($channel) {
			var t = this;
			var text = $channel.data('text');

			ajax.post('removeChannel', {
				channelText: text
			}, {
				ok: function () {
				}
			});

			channelCache = channelCache.filter(function (channel) {
				return channel.label_text !== text;
			});

			t.checkHighlightAfterRemove();

			t.refreshGroup();
		},

		addChannel: function ($channel) {
			var t = this;
			var text = $channel.data('text');

			var found = channelCache.some(function (channel) {
				var found = channel.label_text === text;

				if (found) {
					channel.is_displayed = 't';
				}

				return found;
			});

			if (!found) {
				channelCache.push({
					label_text: text,
					is_displayed: 't'
				});

				channelCache.sort(function (a, b) {
					return a.label_text > b.label_text ? 1 :
						a.label_text < b.label_text ? -1 : 0;
				});
			}

			t.refreshGroup();

			ajax.post('addChannel', {
				channelText: text
			}, {
				ok: function () {
				}
			});

			t.$input.val('');
		},

		refreshGroup: function () {
			var t = this;

			t.$group.html(t.tpls.reviewChannelGroup({
				channels: channelCache,
				showAll: t.showAll,
				highlightText: t.highlightText
			}));
		},

		fetchChannels: function () {
			var t = this;
			var now = +new Date();
			if (now - lastUpdatedMS > updateIntervalMS) {
				ajax.post('getChannels', {}, {
					ok: function (result) {
						channelCache = result.map(function (channelRes) {
							return channelRes.Channel;
						});

						if (isFirstLoad) {
							t.refreshGroup();
							isFirstLoad = false;
						}
					}
				});
				lastUpdatedMS = now;
			}
		},

		destroy: function () {
			var t = this;

			t.$el.off();
			t.off();
		}
	});

	return Channel;
});
