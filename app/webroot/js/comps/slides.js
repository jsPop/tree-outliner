define(['ajax', 'jquery', 'utils', 'lodash', 'text!tpls/common.tpl',
		'backbone', 'mousetrap', 'keymap', 'noty', 'vex-loader',
		'vex', 'help-content', 'md5', 'item',
		'item-list', 'syncer', 'key-manager', 'header-dropdown', 'aui-dropdown2',
		'spinner', 'window-messenger', 'item-processor', 'marked'],
	function (ajax, $, utils, _, commonTplStr,
			  Backbone, Mousetrap, keymap, noty, dialog,
			  vex, helpContent, md5, Item,
			  ItemList, Syncer, KeyManager, HeaderDropdown, _dropdown2,
			  _spinner, messenger, itemProcessor, marked,
			  _undefined) {

		var Slides = Backbone.View.extend({
			events: {},

			initialize: function (opts) {
				var t = this;
				_.extend(t, opts);

				window.$slidesBody = t.loadSlides();

				var head = document.head;
				var script = document.createElement('script');
				script.src = '/js/slides/reveal-with-setup.js';

				head.appendChild(script);
			},

			loadSlides: function () {
				var t = this;

				var root = itemProcessor.getSmartLlist();

				var $slidesBody = $(t.g.tpls.slidesWrapper());

				var $slides = $slidesBody.find('.slides');

				var docTitle = root.content;

				root.children.forEach(function (section) {
					t.loadSlidesSection(section, $slides, docTitle);
					docTitle = null;
				});

				return $slidesBody;
			},

			loadSlidesSection: function (section, $slides, docTitle) {
				var $section = $('<section />');
				$slides.append($section);

				if (docTitle) {
					$section.append('<section>' + marked(docTitle) + '</section>');
				}

				$section.append('<section>' + marked(section.content) + '</section>');

				section.children.forEach(function (subSlide) {
					var subSlideRaws = [subSlide.content];
					subSlide.children.forEach(function (subContent) {
						subSlideRaws.push(subContent.content);
					});
					$section.append('<section>' + marked(subSlideRaws.join('\n\n')) + '</section>');
				});
			}
		})

		return Slides;
	});
