define(['utils', 'underscore', 'jquery', 'mousetrap',
        'keymap'
    ],
    function(utils, _, $, Mousetrap,
             keymap,
        _undefined) {

        var seq = 1;

        var HeaderDropdown = Backbone.View.extend({
            events: {
                'mouseenter': 'onHover',
                'mouseleave': 'onLeave',
                'blur input': 'onInputBlur'
            },
            onHover: function() {
                var t = this;
                _.cancelDelayRun('header-dropdown' + t.seq);
                Mousetrap.pause();
                t.$el.addClass('hovered show-dropdown');
            },
            onLeave: function() {
                var t = this;
                t.$el.removeClass('hovered');
                _.delayRun('header-dropdown' + t.seq, function() {
                    if (!t.$(':focus').length) {
                        Mousetrap.unpause();
                        t.$el.removeClass('show-dropdown');
                    }
                })
            },
            onInputBlur: function(e) {
                var t = this, $input = $(e.currentTarget);
                _.delayRun('header-dropdown' + t.seq, function() {
                    if (!t.$el.is('.hovered') && !t.$(':focus').length) {
                        Mousetrap.unpause();
                        t.$el.removeClass('show-dropdown');
                    }
                });
            },
            initialize: function(opts) {
                var t = this;
                _.extend(t, opts);
                this.seq = seq++;
                t.refresh();
            },
            seq: -1,
            refresh: function() {
            }
        });

        return HeaderDropdown;
});