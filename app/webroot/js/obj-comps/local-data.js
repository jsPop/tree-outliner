define(['utils', 'jquery', 'underscore', 'text!tpls/objects.tpl', 'md5',
        'window-messenger', 'backbone', 'ajax', 'jquery-ui',
        'obj-comps/my-object'],
    function(utils, $, _, tplStr, md5,
             messener, Backbone, ajax, _jqueryUI,
             MyObjectIniter,
             undefined) {

    var _syners = [], MyObject = MyObjectIniter.MyObject;
    var LocalSyncer = {
        sync: function() {
            var isFocus = true, t = this;
            $(window).focus(function() {
                _.each(_syners,function(syncer) {
                    syncer.focus && syncer.focus();
                });
                isFocus = true;
            })
                .blur(function() {
                    _.each(_syners, function (syncer) {
                        syncer.blur && syncer.blur();
                    });
                    isFocus = false;
                });

            $(document).click(function() {
                if (!isFocus) {
                    _.each(_syners,function(syncer) {
                        syncer.focus && syncer.focus();
                    });
                    isFocus = true;
                }
            });

            $(window).on('beforeunload', function() {
                if (isFocus) {
                    _.each(_syners, function (syncer) {
                        syncer.blur && syncer.blur();
                    });
                }
            });

            setInterval(function() {
                if (isFocus) {
                    _.each(_syners, function (syncer) {
                        syncer.blur && syncer.blur();
                    });
                }
            }, 30000);

        },
        bindSync: function(start, focus, blur) {
            _syners.push({
                focus: focus,
                blur: blur
            });
            start && start();
        }
    }

    var tokens = {}, CON_tokenKey = 'objects-tokens', CON_maxTokenNum = 10;
    var token = window.token = {
        getReqParam: function() {
            var tks = {};
            var inUse;

            _.each(tokens.val, function(t) {
                if (!inUse || t.use) inUse = t.token;
                tks[t.token] = 1;
            });

            return {
                all: tks,
                inUse: inUse
            }
        },
        get: function() {
            tokens = utils.getItem(CON_tokenKey, {val:[]});
        },
        create: function() {
            return md5(Math.random());
        },
        set: function(id, token, skipPrint) {
            var prev;
            if (!token) token = this.create();
            tokens.val = _.filter(tokens.val, function(t) {
                if (t.id == id) prev = t;
                return t.id != id;
            });
            for (var i = tokens.val.length; i-- > CON_maxTokenNum;) {
                tokens.val.splice(i, 1);
            }
            tokens.val.push(_.extend({}, prev, {token:token, id: id}));
            utils.setItem(CON_tokenKey, tokens);
            if (!skipPrint) this.print();
        },
        print: function() {
            var res = 'Current tokens in use (NOTE: maximum 10 tokens will be used)\n';
            var inUse;
            for (var i = tokens.val.length; i-- > 0;) {
                var t = tokens.val[i];
                if (!inUse || t.use) {
                    inUse = t;
                }
                res += 'token.set("' + (t.id||'').split('"').join('\"') + '", "' + t.token + '");\n';
            }
            inUse && (res += 'token.use("' + (inUse.id||'').split('"').join('\"') + '");\n');
            console.log(res);
        },
        use: function(id, skipPrint) {
            _.each(tokens.val, function(t) {
                if (t.id == id) t.use = true;
                else t.use = false;
            });
            utils.setItem(CON_tokenKey, tokens);
            if (!skipPrint) this.print();
        },
        remove: function(id, skipPrint) {
            tokens.val = _.filter(tokens.val, function(t) {
                if (t.id == id) prev = t;
                return t.id != id;
            });
            utils.setItem(CON_tokenKey, tokens);
            if (!skipPrint) this.print();
        }
    };


    var aliasMap, CON_aliasKey = 'objects-aliases', CON_maxAlias = 2000;
    var aliasManager = window.alias = {
        add: function(alias, snapshot) {
            if (snapshot instanceof MyObject) {
                snapshot = _.extend({}, {
                    id: snapshot.d.id,
                    name: snapshot.d._name_,
                    space: snapshot.d._space_,
                    descr: snapshot.d._descr_
                });
            }

            aliasMap.val.push({
                alias: alias,
                snapshot: snapshot
            });

            if (aliasMap.val.length > CON_maxAlias) {
                while (aliasMap.val.length > CON_maxAlias * 2 / 3) {
                    aliasMap.val.unshift();
                }
            }
            this.set();
        },
        a: function(alias) {
            var result = this.alias(alias);

            return _.map(result, function(a) {
                return app.get(a.snapshot.id);
            })
        },
        alias: function(alias) {
            var result = [], t = this;
            _.each(aliasMap.val, function(a, idx) {
                if (a.alias == alias) {
                    a.idx = idx;
                    result.push(a);
                }
            });
            t.dedup(result);
            return result;
        },
        aliasStarts: function(start) {
            var result = [], lenStart = start.length, t = this;
            _.each(aliasMap.val, function(a, idx) {
                if (a.alias.length != lenStart && a.alias.substr(0, lenStart) == start) {
                    a.idx = idx;
                    result.push(a);
                }
            });
            t.dedup(result);
            return result;
        },
        dedup: function(results) {
            var idHash = {}, resToRemove = [];
            _.each(results, function(res, idx) {
                if (idHash[res.alias + '|' + res.snapshot.id]) {
                    resToRemove.push(idx);
                } else {
                    idHash[res.alias + '|' + res.snapshot.id] = 1;
                }
            });

            var removedIdx = {};
            var toReset = resToRemove.length;
            for (var i = resToRemove.length; i-- > 0;) {
                var res = results.splice(resToRemove[i], 1);
                if (!removedIdx[res[0].idx]) {
                    aliasMap.val.splice(res[0].idx, 1);
                    removedIdx[res[0].idx];
                }
            }
            if (toReset) this.set();
        },
        get: function() {
            aliasMap = utils.getItem(CON_aliasKey, {val:[]});
        },
        set: function() {
            utils.setItem(CON_aliasKey, aliasMap);
        },
        remove: function(alias, from, len) {
            var alias = aliasManager.alias(alias);
            from = from || 0;
            len = len || alias.length;
            var to = from + len;
            for (var i = to; i-- > from; ) {
                if (alias[i]) aliasMap.val.splice(alias[i].idx, 1);
            }
            this.set();
        }
    };





    var shortcuts = window.sh = window.shortcuts = {
        get: function(key) {
            return app.get(this['$_' + key]);
        },
        load: function(key, cb) {
            return app.load(this['$_' + key], cb);
        }
    };
    shortcuts.g = shortcuts.get;
    shortcuts.l = shortcuts.load;

    LocalSyncer.sync();
    LocalSyncer.bindSync(function() {
        token.get();
        if (!tokens.val.length) token.set('default', null, true);
        token.print();
    }, function() {
        token.get();
    });


    LocalSyncer.bindSync(function() {
        aliasManager.get();
    }, function() {
        aliasManager.get();
    }, function() {
        aliasManager.set();
    });

        var app;
        return function(_app) {
            app = _app;

            LocalSyncer.bindSync(function() {
                app.getCache();
            }, function() {
                app.getCache();
            }, function() {
                app.setCache();
            })

            return {
                token: token,
                aliasManager: aliasManager
            }
        }

})