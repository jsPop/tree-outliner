define(['text!tpls/help-details.tpl', 'utils', 'marked'],
	function (helpDetailsTplStr, utils, marked) {

	var helpDetails = utils.parseTpls(helpDetailsTplStr);

	var helpContent = [
		{
			title: '基础操作',
			items: [{
				key: 'list-dialog',
				title: '打开文档列表对话框',
				shortcut: ['a a'],
				weight: 60
			}, {
				key: 'create-dialog',
				title: '新建文档',
				shortcut: ['a a'],
				weight: 60
			}, {
				key: 'help-dialog',
				title: '打开帮助对话框',
				shortcut: ['f1'],
				weight: 30
			}, {
				key: 'edit-current-item',
				title: '编辑当前选中节点',
				shortcut: ['e', 'f2'],
				weight: 40
			}, {
				key: 'del-current-item',
				title: '删除当前选中节点',
				shortcut: ['a g', 'del'],
				weight: 40
			}, {
				key: 'search-item',
				title: '搜索节点',
				shortcut: ['/', 'cmd+f', 'ctrl+f'],
				weight: 30
			}, {
				key: 'search-document',
				title: '快速搜索文档',
				shortcut: ['cmd+p', 'ctrl+p'],
				weight: 30
			}, {
				key: 'toggle-learning-mode',
				title: '切换学习模式',
				shortcut: ['`'],
				weight: 30
			}, {
				key: 'toggle-tag-state',
				title: '快速切换标签状态',
				shortcut: ['space'],
				weight: 30
			}, {
				key: 'share-dialog',
				title: '打开节点分享对话框',
				shortcut: ['z'],
				weight: 30
			}, {
				key: 'close-dialog',
				title: '关闭对话框',
				shortcut: ['escape'],
				weight: 40
			}, {
				key: 'sync-document',
				title: '立即同步文档',
				shortcut: ['g'],
				weight: 20
			}]
		},
		{
			title: '创建内容节点',
			items: [{
				key: 'create-child-item',
				title: '创建子内容节点',
				shortcut: ['cmd+d', 'ctrl+d'],
				weight: 30
			}, {
				key: 'create-item-beneath',
				title: '创建同级节点 (下方)',
				shortcut: ['enter'],
				weight: 30
			}, {
				key: 'create-item-above',
				title: '创建同级节点 (上方)',
				shortcut: ['shift+enter'],
				weight: 30
			}, {
				key: 'create-parent-item',
				title: '创建父级节点, 将选中的节点建立群组',
				shortcut: ['cmd+s', 'ctrl+s'],
				weight: 40
			}]
		},
		{
			title: '选择内容节点',
			items: [{
				key: 'toggle-select-item',
				title: '切换节点选择',
				shortcut: ['w'],
				weight: 10
			}, {
				key: 'cancel-all-select',
				title: '取消当前所有选择',
				shortcut: ['a w'],
				weight: 10
			}, {
				key: 'up-batch-select',
				title: '向上批量选择节点',
				shortcut: ['shift+i', 'shift+up'],
				weight: 30
			}, {
				key: 'down-batch-select',
				title: '向下批量选择节点',
				shortcut: ['shift+k', 'shift+down'],
				weight: 30
			}, {
				key: 'batch-select-same-level',
				title: '批量选择同级节点',
				shortcut: ['shift+j', 'shift+left'],
				weight: 30
			}, {
				key: 'batch-select-item',
				title: '批量选择子级节点',
				shortcut: ['shift+l', 'shift+right'],
				weight: 30
			}]
		},
		{
			title: '修改节点',
			items: [{
				key: 'copy-item-to-current-item',
				title: '复制选中的节点到当前节点下',
				shortcut: ['a c'],
				weight: 40
			}, {
				key: 'move-item-to-current-item',
				title: '移动选中的节点到当前节点下',
				shortcut: ['a m', 'm'],
				weight: 40
			}, {
				key: 'save-edit',
				title: '保存节点修改 (编辑状态)',
				shortcut: ['cmd+enter'],
				weight: 120
			}, {
				key: 'cancel-edit',
				title: '取消节点修改 (编辑状态)',
				shortcut: ['esc'],
				weight: 120
			},{
				key: 'multi-line-text',
				title: '内容编辑换行 (多行文本)',
				shortcut: ['alt+enter'],
				weight: 60
			}, {
				key: 'reg-divide-item',
				title: '用正则表达式将节点打散成一系列节点',
				shortcut: ['cmd+;', 'ctrl+;'],
				weight: 40
			}]
		},
		{
			title: '节点移动',
			items: [{
				key: 'move-up-item',
				title: '向上移动节点',
				shortcut: ['cmd+i', 'ctrl+i', 'cmd+up', 'ctrl+up'],
				weight: 30
			}, {
				key: 'move-down-item',
				title: '向下移动节点',
				shortcut: ['cmd+k', 'ctrl+k', 'cmd+down', 'ctrl+down'],
				weight: 30
			}, {
				key: 'draw-back-item',
				title: '反缩进节点',
				shortcut: ['cmd+j', 'ctrl+j', 'shift+tab'],
				weight: 30
			}, {
				key: 'indent-item',
				title: '缩进节点',
				shortcut: ['cmd+l', 'ctrl+l', 'tab'],
				weight: 30
			}]
		},
		{
			title: '光标移动',
			items: [{
				key: 'move-up-cursor',
				title: '向上移动光标',
				shortcut: ['i', 'up'],
				weight: 30
			}, {
				key: 'move-down-cursor',
				title: '向下移动光标',
				shortcut: ['k', 'down'],
				weight: 30
			}, {
				key: 'move-left-cursor',
				title: '向左移动光标',
				shortcut: ['j', 'left'],
				weight: 30
			}, {
				key: 'move-right-cursor',
				title: '向右移动光标',
				shortcut: ['l', 'right'],
				weight: 30
			}]
		},
		{
			title: '文档列表管理',
			items: [{
				key: 'create-document-dialog-link',
				title: '创建一个指向该文档的快捷链接',
				shortcut: ['a e'],
				weight: 10
			}, {
				key: 'item-to-document',
				title: '把选中的节点转换成一个文档',
				shortcut: ['a d'],
				weight: 10
			}, {
				key: 'bookmark-document',
				title: '收藏文档',
				weight: 20
			}]
		},
		{
			title: '控制内容的展开 / 缩起',
			items: [{
				key: 'toggle-item',
				title: '展缩子级节点',
				shortcut: ['d'],
				weight: 30
			}, {
				key: 'toggle-current-item',
				title: '展缩当前节点',
				shortcut: ['f'],
				weight: 50
			}, {
				key: 'toggle-same-level-item',
				title: '展缩同级节点',
				shortcut: ['r'],
				weight: 30
			}, {
				key: 'toggle-parent-item',
				title: '缩起父级节点',
				shortcut: ['s'],
				weight: 50
			}, {
				key: 'item-to-level-1',
				title: '收起当前节点到它的第一级',
				shortcut: ['1'],
				weight: 40
			}, {
				key: 'item-to-level-2',
				title: '收起当前节点到它的第二级',
				shortcut: ['2'],
				weight: 30
			}, {
				key: 'item-to-level-3',
				title: '收起当前节点到它的第三级',
				shortcut: ['3'],
				weight: 30
			}, {
				key: 'item-to-level-4',
				title: '收起当前节点到它的第四级',
				shortcut: ['4'],
				weight: 40
			}, {
				key: 'document-top-to-level-1',
				title: '文档顶层收缩至第一层',
				shortcut: ['cmd+1', 'ctrl+1'],
				weight: 40
			}, {
				key: 'document-top-to-level-2',
				title: '文档顶层收缩至第二层',
				shortcut: ['cmd+2', 'ctrl+2'],
				weight: 30
			}, {
				key: 'document-top-to-level-3',
				title: '文档顶层收缩至第三层',
				shortcut: ['cmd+3', 'ctrl+3'],
				weight: 30
			}, {
				key: 'document-top-to-level-4',
				title: '文档顶层收缩至第四层',
				shortcut: ['cmd+4', 'ctrl+4'],
				weight: 40
			}]
		},
		{
			title: '任务管理',
			items: [{
				key: 'fast-insert-question-tag',
				title: '快速插入 question 标签',
				shortcut: ['shift+/'],
				weight: 40
			}, {
				key: 'fast-insert-task-tag',
				title: '快速插入 task 标签',
				shortcut: ['shift+1'],
				weight: 40
			}, {
				key: 'toggle-task-tag',
				title: '切换任务 (问题) 状态',
				shortcut: ['space'],
				weight: 100
			}]
		},
		{
			title: '其他操作',
			items: [{
				key: 'export-dialog',
				title: '将内容作为模板使用',
				shortcut: ['c'],
				weight: 10
			}, {
				key: 'import-dialog',
				title: '导入内容',
				shortcut: ['v'],
				weight: 10
			}, {
				key: 'item-id-dialog',
				title: '创建节点内链',
				shortcut: ['a r'],
				weight: 40
			}]
		}
	];

	helpContent.forEach(function (section) {
		section.items.forEach(function (item) {
			if (helpDetails[item.key]) {
				item.detailHtml = marked(helpDetails[item.key]());
			}
			else {
				item.detailHtml = '';
			}
			item.shortcut = item.shortcut || [];
		});
	});

	return helpContent;
});
