/*<MAIN_SCRIPT>*/

/*<DEV*/
require([ '/js/main.js' ], function() {
    /*DEV>*/
    require([ 'ajax', 'jquery', 'utils', 'underscore', 'text!tpls/review.tpl',
            'backbone', 'keymap', 'noty', 'md5', 'moment',
        'spinner'],
        function(ajax, $, utils, _, reviewTpls,
                 Backbone, keymap, noty, md5, moment,
                 _spinner,
                 _undefined) {

            $(function() {

                var tpls = utils.parseTpls(reviewTpls);
                var $window = $(window);
                var $document = $(document), CON_bottomMargin = 240,
                    CON_maxItemNum = 80, CON_itemAfterRemoval = 50,
                    CON_responseNum = 10;

                $('#item-spinner').spin('large');

                var Review = Backbone.View.extend({
                    events: {
                        'click [data-item]': 'onSelect',
                        'click footer .more': 'onToggleMore',
                        'click footer [data-time]': 'onReview',
                        'click': 'onToggleOffMore',
                        'click footer .button.goto-list': 'onGotoList',
                        'click footer .button.remove': 'onRemove',
                        'click .setting-toggle': 'onToggleSettings',
                        'click .field': 'onClickOnField'
                    },
                    initialize: function(opts) {
                        var t = this;
                        _.extend(t, opts);

                        t.$itemWrappers = [];

                        t.$itemContainer = t.$('#items');
                        t.$mainActions = t.$('footer .main');
                        t.$expandedActions = t.$('footer .expanded');
                        t.$timeActions = t.$('#hidden-zone .time-actions');

                        t.$footer = t.$('footer');
                        t.$settings = t.$('.settings');

                        t.initButtons();
                        t.initEvents();
                    },
                    initButtons: function() {
                        var t = this;
                        t.$('.button.button-select:not(.setup)').each(function() {
                            var $t = $(this);
                            $t.prepend('<span class="fa fa-check-square-o"></span><span class="fa fa-square-o"></span>')
                                .addClass('setup');
                            $t.click(function() {
                                $t.toggleClass('selected', !$t.is('.selected'));
                            });
                        });
                    },
                    initEvents: function() {
                        var t = this;
                        $window.scroll(_.throttle(function() {

                            if (!t._autoScrolling) {
                                var height = $window.height(),
                                    docHeight = $document.height(),
                                    scrollTop = $window.scrollTop();
                                if (scrollTop + height > docHeight - height * 2) {
                                    t.getItem();
                                }
                                t.checkOnScroll(scrollTop, height);
                            }
                        }, 350, {
                            trailing: true
                        }));

                    },

                    onToggleSettings: function(e) {
                        var t = this;
                        t.$el.toggleClass('show-settings');
                    },

                    _isActionExpanded: false,
                    onToggleMore: function(e) {
                        var t = this;
                        t.toggleMore();
                        e.stopPropagation();
                    },
                    toggleMore: function(expanded) {
                        var t = this;
                        var mainHeight = t.$mainActions.outerHeight();

                        if (typeof expanded == 'boolean') {
                            t._isActionExpanded = !expanded;
                        }

                        if (!t._isActionExpanded) {
                            t.$expandedActions.css('bottom', mainHeight);
                        } else {
                            t.$expandedActions.css('bottom', -600);
                        }

                        t._isActionExpanded = !t._isActionExpanded;
                    },
                    onToggleOffMore: function() {
                        this.toggleMore(false);
                    },

                    onRemove: function() {
                        var t = this;
                        if (!t.$selected) return;

                        var removed = !t.$selected.is('.removed'),
                            hasRemoved = !t.$selected.is('.has-removed');

                        var itemId = t.$selected.data('item');
                        ajax.post('index_item', {
                            now: new Date().getTime(),
                            removed: removed,
                            itemId: itemId
                        }, {ok: function() {
                            t.$('[data-item=' + itemId + ']')
                                .toggleClass('removed', removed);
                        }});

                        t.$('[data-item=' + itemId + ']')
                            .toggleClass('has-removed', removed);
                        t.refreshActions();
                        t.hoverOnNext();
                    },

                    onClickOnField: function(e) {
                        var t = this, $t = $(e.currentTarget);

                        $t.toggleClass('seen', !$t.is('.seen'));
                    },

                    onGotoList: function(e) {
                        var t = this, $t = $(e.currentTarget);

                        if (!t.$selected) return;
                        var listId = t.$selected.data('list'),
                            itemId = t.$selected.data('item'),
                            href = location.origin + '/app/list/' + listId;

                        $t.attr({
                            href: href + '/' + itemId,
                            target: md5(href)
                        });
                    },

                    onReview: function(e) {
                        var t = this, $t = $(e.currentTarget);
                        var isQuickReview = $t.is('.quick-review');
                        if (!t.$selected) return;

                        var itemId = t.$selected.data('item');
                        var time = $t.data('time');
                        var now = moment();
                        var type = time.indexOf('h') != -1 ? 'hours' :
                            time.indexOf('d') != -1 ? 'days' :
                                time.indexOf('w') != -1 ? 'weeks' :
                                    time.indexOf('n') != -1 ? 'months' : 'years';

                        var currTime = new Date().getTime();
                        var delta = now.add(time.replace(/[wdhny]/g, ''), type).toDate().getTime()
                                - currTime;
                        var reviewTime = Math.ceil(delta * Math.random() * 1/3) +
                                Math.ceil(delta * 2 / 3) + currTime;

                        console.log(moment(reviewTime).toString());

                        var isNewReview = !t.$selected.is('.reviewed');

                        ajax.post('review', {
                            itemId: itemId,
                            nextReview: reviewTime,
                            reviewType: !isQuickReview ? time : t.$selected.data('reviewType'),
                            now: new Date().getTime(),
                            isNewReview: isNewReview
                        }, {
                            ok: function(rsp) {
                                t.$('[data-item="' + itemId + '"]')
                                    .addClass('reviewed');
                            }
                        });

                        t.$('[data-item="' + itemId + '"]').data('reviewType', time);
                        t.refreshActions();
                        t.hoverOnNext();
                    },

                    hoverOnNext: function() {
                        var t = this;
                        if (t.$selected.is('.removed') || t.$selected.is('.reviewed')) return;
                        t.hoverOn(
                            t.$selected.nextAll('[data-item]:not(.remove):not(.reviewed)').eq(0)
                        );
                    },

                    _autoScrolling: false,
                    hoverOn: function($item) {
                        var t = this;
                        if (!$item || !$item.length) return;
                        var top = $item.find('.item').offset().top;

                        t._autoScrolling = true;
                        $('html, body').stop().animate({
                            scrollTop: top - $window.height() * 3 / 7
                        }, 500, function() {
                            t._autoScrolling = false;
                        });
                        t.select($item);
                    },

                    onSelect: function(e) {
                        var $t = $(e.currentTarget);

                        this.select($t);
                    },

                    select: function($itemWrapper) {
                        var t = this;
                        if (!$itemWrapper) return;
                        t.$itemContainer.children('.selected').removeClass('selected');
                        $itemWrapper.addClass('selected');
                        t.$selected = $itemWrapper;
                        t.refreshActions();
                    },

                    refreshActions: function() {
                        var t = this;

                        if (!t.$selected) return;

                        var reviewType = t.$selected.data('reviewType'),
                            removed = t.$selected.is('.has-removed');

                        t.$mainActions.toggleClass('removed', removed);

                        var $time = t.$timeActions.find('[data-time=' +
                            (reviewType == '2h' || !reviewType ? '4h' : reviewType) + ']');


                        if (!$time.next().length) { $time = $time.prev(); }
                        else if (!$time.prev().length) { $time = $time.next(); }

                        t.$mainActions.find('[data-time]:not(.quick-review)').remove();
                        t.$mainActions.find('.quick-review')
                            .after($time.next().clone())
                            .after($time.clone())
                            .after($time.prev().clone());

                        t.$footer.find('.highlighted').removeClass('highlighted');
                        t.$footer.find('[data-time=' + reviewType + ']').addClass('highlighted');
                    },

                    checkOnScroll: function(scrollTop, winHeight) {
                        var t = this, $currItem, deltaTop;

                        var getCurrItem = function() {
                            var $curr;
                            _.some(t.$itemWrappers, function ($item) {
                                var top = $item.offset().top;
                                if (top > scrollTop + 1/4 * winHeight - 20) {
                                    $curr = $item;
                                    return true;
                                }
                            });
                            return $curr;
                        }

                        if (t.$selected) {
                            var selectedTop = t.$selected.find('.item').offset().top,
                                selectedHeight = t.$selected.find('.item').outerHeight();
                        }

                        if (!t.$selected || selectedTop > scrollTop + winHeight ||
                            selectedTop + selectedHeight < scrollTop + 1/4 * winHeight) {
                            $currItem = getCurrItem();
                            t.select($currItem);
                        }

                        if (t.$itemWrappers.length > CON_maxItemNum) {
                            $currItem = $currItem || getCurrItem();
                            if (!$currItem) return;
                            deltaTop = scrollTop - $currItem.position().top;

                            var $removed = t.$itemWrappers.splice(0, t.$itemWrappers.length - CON_itemAfterRemoval);

                            _.each($removed, function($item) {
                                $item.remove();
                            });

                            t.$itemContainer.children('h2').each(function() {
                                var $t = $(this);
                                if ($t.next().is('h2')) {
                                    $t.remove();
                                }
                            });

                            if ($currItem) {
                                var top = $currItem.position().top;
                                $window.scrollTop(top + deltaTop);
                            }
                        }
                    },

                    parseFields: function($itemWrapper) {
                        var t = this, CON_ratio = 1 / 2,
                            $fields = $itemWrapper.find('.field');

                        $fields.each(function() {
                            this.rand = Math.random();
                        });

                        $fields.sort(function(a, b) {
                            return a.rand - b.rand;
                        });

                        var len = Math.floor(CON_ratio * $fields.length);
                        for (var i = 0; i < len; i ++) {
                            $fields.eq(0).addClass('seen');
                        }
                    },

                    _gettingItems: false,
                    getItem: function() {
                        var t = this;
                        if (t._gettingItems) {
                            return;
                        }
                        t._gettingItems = true;

                        if (t.$el.is('.no-more-review')) return;

                        ajax.post('get_review_items', {
                            req: JSON.stringify({
                                tags:
                                    [],
//                                ['elem.entity'],
                                now: new Date().getTime()
                            })
                        }, {
                            last: function() {
                                t._gettingItems = false;
                            },
                            ok: function(rsp) {
//                                console.log(rsp);

                                var hash = {};
                                _.each(rsp.items, function(item) {
                                    item.content = item.content || '';
                                    hash[item.id] = item;
                                    item.children = [];
                                });

                                // remaining items
                                var remaining = tpls.itemCount(rsp);
                                t.$itemContainer.append(remaining);

                                // rendering the items per list
                                var parentHash = {};
                                _.each(rsp.arounds, function(item) {
                                    item.content = item.content || '';
                                    var p = hash[item.parent_item_id];
                                    if (p) {
                                        hash[item.parent_item_id].children.push(item);
                                    }
                                    parentHash[item.id] = item;
                                });

                                rsp.items.sort(function(a, b) {
                                    if (a.list_id > b.list_id) return 1;
                                    if (a.list_id < b.list_id) return -1;
                                    return 0;
                                });

                                var listHash = {};
                                _.each(rsp.lists, function(list) {
                                    listHash[list.id] = list;
                                });

                                var currList = false;
                                _.each(rsp.items, function(item) {
                                    var $itemWrapper;

                                    var p = parentHash[item.parent_item_id];

                                    if (!currList || currList.id != item.list_id) {
                                        currList = listHash[item.list_id];
                                        currList.md5 = md5(location.origin + '/app/list/' + currList.id);

                                        t.$itemContainer.append(tpls.list(currList));
                                    }

                                    var reviewTimes = Number(item.review_times);
                                    reviewTimes = isNaN(reviewTimes) ? 0 : reviewTimes;

                                    var now = moment(new Date());
                                    $itemWrapper = $(tpls.item({
                                        itemId: item.id,
                                        parent: p && utils.getStandaloneBody(p.content)[0].outerHTML,
                                        item: utils.getStandaloneBody(item.content)[0].outerHTML,
                                        children: _.map(item.children, function (child) {
                                            return utils.getStandaloneBody(child.content)[0].outerHTML
                                        }).join(''),
                                        listId: item.list_id,
                                        reviewed: item.reviewed && moment(Number(item.reviewed)).from(now),
                                        created: item.created && moment(Number(item.created)).from(now),
                                        reviewTimes: reviewTimes || 0,
                                        reviewType: item.review_type || ''
                                    }));

                                    // 允许点击跳转链接
                                    // $itemWrapper.find('a:not(.embedded-image-link):not(.field)').each(function () {
                                    //     var $t = $(this);
                                    //     $t.after($('<span/>').html(
                                    //             $t.html()).attr('data-href', $t.attr('href'))
                                    //     ).remove();
                                    // });

                                    t.$itemContainer.append($itemWrapper);


                                    $itemWrapper.data('top', $itemWrapper.position().top);
                                    $itemWrapper.data('height', $itemWrapper.outerHeight());
                                    t.$itemWrappers.push($itemWrapper);

                                    if (t.$settings.find('.interactive-mode').is('.selected')) {
                                        t.parseFields($itemWrapper);
                                    } else {
                                        $itemWrapper.find('.field').addClass('seen');
                                    }
                                });

                                if (rsp.items.length < CON_responseNum) {
                                    t.$el.addClass('no-more-review');
                                }
                            }
                        })
                    },
                    hash: null
                });


                var review = new Review({
                    $el: $('#page')
                });

                review.getItem();
            })
        });
    /*<DEV*/
});
/*DEV>*/
