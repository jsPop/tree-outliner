/*<MAIN_SCRIPT>*/

/*<DEV*/
require([ '/js/main.js' ], function() {
    /*DEV>*/
    require([ 'ajax', 'jquery', 'utils', 'underscore', 'text!tpls/indexed-items.tpl',
            'backbone', 'keymap', 'noty', 'md5', 'moment',
            'spinner'],
        function(ajax, $, utils, _, indexedItemTplStr,
                 Backbone, keymap, noty, md5, moment,
                 _spinner,
                 _undefined) {
            'use strict';

            var tpls = utils.parseTpls(indexedItemTplStr);
            var $window = $(window);

            $(function() {
                ajax.post('get_indexed_items', function(res) {

                });
            });
        });
    /*<DEV*/
});
/*DEV>*/