{
	appDir: "../webroot/",
	baseUrl: "js",
	dir: "../prod-root/",
	mainConfigFile: '../webroot/js/main.js',
	modules: [
		{name: "pages/app"},
		{name: "pages/user"}
	]
}
		