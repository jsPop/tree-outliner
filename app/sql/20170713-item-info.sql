alter table tr_items
add column created_by integer
references tr_users(id);

alter table tr_items
add column updated_by INTEGER
REFERENCES tr_users(id);
