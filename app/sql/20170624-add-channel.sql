-- 请务必确认部署的环境不要重复执行 sql

alter table tr_indexed_items
add column channel varchar(50);

CREATE TABLE IF NOT EXISTS `tr_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label_text` varchar(50) NOT NULL,
  `owner_id` int(11) default null,
  `is_displayed` varchar(2),
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
