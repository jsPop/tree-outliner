var gulp = require('gulp');
var replace = require('gulp-replace');
var fs = require('fs');

gulp.task('set-expire', function () {
	return gulp.src([
		'**/webroot/.htaccess',
	])
		.pipe(replace(/ExpiresDefault "access plus 1 second"/g,
			'ExpiresDefault "access plus 10 years" \n' +
			'Header set Cache-Control "max-age=315360000, public"'))
		.pipe(gulp.dest(''));
});

gulp.task('replace-dev', function() {
	return gulp.src([
		'**/webroot/js/main.js',
		'**/Config/core.php',
		'**/Config/bootstrap.php',
		'**/Config/database.php',
		'**/webroot/js/pages/app.js',
		'**/webroot/js/pages/page.js',
		'**/webroot/js/pages/popup.js',
		'**/webroot/js/pages/review.js',
		'**/webroot/js/pages/objects.js',
		'**/webroot/js/pages/page-template.js',
		'**/webroot/js/pages/user.js',
	])
		.pipe(replace(/\/\*<DEV\*\//g, '/*<DEV'))
		.pipe(replace(/\/\*DEV>\*\//g, 'DEV>*/'))
		.pipe(replace(/\/\*<PROD(?!\*)/g, '/*<PROD*/'))
		.pipe(replace(/(^|[^*])PROD>\*\//g, '$1/*PROD>*/'))
		.pipe(replace(/##APP_DEPLOY_TIME##/g, new Date().getTime()))
		.pipe(gulp.dest(''));
});

gulp.task('concat-js', ['replace-dev'], function() {
	var mainScriptContent = fs.readFileSync(__dirname + '/webroot/js/main.js').toString();

	return gulp.src([
		'**/webroot/js/pages/app.js',
		'**/webroot/js/pages/page.js',
		'**/webroot/js/pages/popup.js',
		'**/webroot/js/pages/review.js',
		'**/webroot/js/pages/objects.js',
		'**/webroot/js/pages/page-template.js',
		'**/webroot/js/pages/user.js',
	])
		.pipe(replace(/\/\*<MAIN_SCRIPT>\*\//, mainScriptContent))
		.pipe(gulp.dest(''));
})

gulp.task('deploy', ['set-expire', 'concat-js']);
