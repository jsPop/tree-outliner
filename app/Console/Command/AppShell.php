<?php
/**
 * AppShell file
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Shell', 'Console');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class AppShell extends Shell {
	public function l($msg, $data = null, $type = LOG_DEBUG, $scope = NULL) {
//		if ($data === null) {
//			$data = &$msg;
//			unset($msg);
//			$msg = null;
//		}
//		if ($msg != null) {
//			$this->log(array('msg' => &$msg, 'data' => &$data), $type, $scope);
//		} else {
//			$this->log($data, $type, 'bla');
//		}
	}

	public function le($msg, $data = null, $scope = NULL) {
		$this->l($msg, $data, LOG_ERR, $scope);
	}
}
