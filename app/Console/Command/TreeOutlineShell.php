<?php

APP::uses('Folder', 'Utility');

class DeployPackShell extends AppShell {

	function main() {
		$dir = APP;
		$deployPath = "$dir/tmp/deploy";

        $this->CON_mainScript = "$deployPath/webroot/js/main.js";

		$deploy = new Folder($deployPath);
		$deploy->delete();

		$appFolder = new Folder($dir);
		$appFolder->copy(array(
				'to' => $deployPath,
				'skip' => array('tmp'),
		));


		$this->_parseFiles(array(
			"$deployPath/webroot/js/main.js",
			"$deployPath/Config/core.php",
            "$deployPath/Config/bootstrap.php",
			"$deployPath/Config/database.php",
		), array(
			array("$deployPath/webroot/.htaccess", array('expire' => 1)),
            array("$deployPath/webroot/js/pages/app.js",array('concatMain' => 1)),
            array("$deployPath/webroot/js/pages/popup.js",array('concatMain' => 1)),
            array("$deployPath/webroot/js/pages/user.js",array('concatMain' => 1)),
		));

		chdir("$dir/tmp/deploy/build-scripts");
		exec("r.js -o fs-tube-build.js", $outputs);
		$this->l($outputs);

 		(new Folder("$deployPath/webroot"))->delete();
		rename("$deployPath/prod-root", "$deployPath/webroot");

		unlink("$deployPath/Config/database.php");
		rename("$deployPath/Config/database.prod.php", "$deployPath/Config/database.php");

		chdir("$deployPath/..");
		exec("zip -r9 $dir/tmp/deploy.zip deploy", $outpus);
		$this->l($outpus);

		(new Folder($deployPath))->delete();
	}

    var $CON_mainScript;

    function _parseFiles($files, $extras = array()) {
        foreach($files as $file) {
            $content = file_get_contents($file);
            $content = $this->_parseContent($content);
            file_put_contents($file, $content);
        }

        $main = file_get_contents($this->CON_mainScript);

        foreach($extras as $extra) {
            $content = file_get_contents($extra[0]);
            $content = $this->_parseContent($content, $extra[1]);

            if($extra[1]['concatMain']) {
                file_put_contents($extra[0], $main ."\n". $content);
            } else {
                file_put_contents($extra[0], $content);
            }
        }
    }

	function _parseContent($content, $opts = array()) {
		$content = preg_replace('!/\*<DEV\*/!', '/*<DEV', $content);
		$content = preg_replace('!/\*DEV>\*/!', 'DEV>*/', $content);
		$content = preg_replace('!/\*<PROD!', '/*<PROD*/', $content);
		$content = preg_replace('!PROD>\*/!', '/*PROD>*/', $content);
		if ($opts['expire']) {
			$content = str_replace('ExpiresDefault "access plus 1 second"',
					'ExpiresDefault "access plus 10 years"
	 Header set Cache-Control "max-age=315360000, public"', $content);
		}
		$deployTime = time(true);
		$content = preg_replace('!##APP_DEPLOY_TIME##!', $deployTime, $content);
		return $content;
	}

}