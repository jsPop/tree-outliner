PGDMP     ,    8                r            tree_outliner    8.4.21    8.4.21 9    @           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            A           0    0 
   STDSTRINGS 
   STDSTRINGS     )   SET standard_conforming_strings = 'off';
                       false            B           1262    17475    tree_outliner    DATABASE        CREATE DATABASE tree_outliner WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_AU.UTF-8' LC_CTYPE = 'en_AU.UTF-8';
    DROP DATABASE tree_outliner;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             lzheng    false            C           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  lzheng    false    3            �            1259    17558    tr_bookmarks    TABLE     �   CREATE TABLE tr_bookmarks (
    id integer NOT NULL,
    user_id integer,
    item_id character varying(40) DEFAULT NULL::character varying
);
     DROP TABLE public.tr_bookmarks;
       public         postgres    false    1814    3            �            1259    17556    tr_bookmarks_id_seq    SEQUENCE     u   CREATE SEQUENCE tr_bookmarks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tr_bookmarks_id_seq;
       public       postgres    false    141    3            D           0    0    tr_bookmarks_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE tr_bookmarks_id_seq OWNED BY tr_bookmarks.id;
            public       postgres    false    140            E           0    0    tr_bookmarks_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('tr_bookmarks_id_seq', 1, false);
            public       postgres    false    140            �            1259    17567    tr_histories    TABLE     �   CREATE TABLE tr_histories (
    id integer NOT NULL,
    updated timestamp without time zone,
    created timestamp without time zone,
    user_id integer,
    item_id character varying(40) DEFAULT NULL::character varying
);
     DROP TABLE public.tr_histories;
       public         postgres    false    1816    3            �            1259    17565    tr_histories_id_seq    SEQUENCE     u   CREATE SEQUENCE tr_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 *   DROP SEQUENCE public.tr_histories_id_seq;
       public       postgres    false    3    143            F           0    0    tr_histories_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE tr_histories_id_seq OWNED BY tr_histories.id;
            public       postgres    false    142            G           0    0    tr_histories_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('tr_histories_id_seq', 1, false);
            public       postgres    false    142            �            1259    17576    tr_item_lists    TABLE     i  CREATE TABLE tr_item_lists (
    id integer NOT NULL,
    title character varying(200) DEFAULT NULL::character varying,
    owner_id integer,
    removed timestamp without time zone,
    created timestamp without time zone,
    updated timestamp without time zone,
    synced bigint,
    token_expiry bigint,
    current_token integer,
    token_seq integer
);
 !   DROP TABLE public.tr_item_lists;
       public         postgres    false    1818    3            �            1259    17574    tr_item_lists_id_seq    SEQUENCE     v   CREATE SEQUENCE tr_item_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 +   DROP SEQUENCE public.tr_item_lists_id_seq;
       public       postgres    false    3    145            H           0    0    tr_item_lists_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE tr_item_lists_id_seq OWNED BY tr_item_lists.id;
            public       postgres    false    144            I           0    0    tr_item_lists_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('tr_item_lists_id_seq', 1, true);
            public       postgres    false    144            �            1259    17583    tr_items    TABLE     �  CREATE TABLE tr_items (
    id character varying(40) NOT NULL,
    content text,
    parent_item_id character varying(40) DEFAULT NULL::character varying,
    list_id integer,
    is_orphan integer,
    type character varying(5) DEFAULT NULL::character varying,
    removed timestamp without time zone,
    is_hidden integer,
    updated timestamp without time zone,
    created timestamp without time zone,
    task_points integer,
    ref_item_id character varying(40) DEFAULT NULL::character varying,
    image text,
    task_status character varying(4) DEFAULT NULL::character varying,
    synced bigint,
    seq integer,
    id_seq integer,
    user_hash character varying(40) DEFAULT NULL::character varying,
    remove_attempt integer
);
    DROP TABLE public.tr_items;
       public         postgres    false    1819    1820    1821    1822    1823    3            �            1259    17598    tr_list_shares    TABLE     c   CREATE TABLE tr_list_shares (
    id integer NOT NULL,
    user_id integer,
    list_id integer
);
 "   DROP TABLE public.tr_list_shares;
       public         postgres    false    3            �            1259    17596    tr_list_shares_id_seq    SEQUENCE     w   CREATE SEQUENCE tr_list_shares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.tr_list_shares_id_seq;
       public       postgres    false    3    148            J           0    0    tr_list_shares_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE tr_list_shares_id_seq OWNED BY tr_list_shares.id;
            public       postgres    false    147            K           0    0    tr_list_shares_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('tr_list_shares_id_seq', 1, false);
            public       postgres    false    147            �            1259    17606    tr_tags    TABLE     �   CREATE TABLE tr_tags (
    id integer NOT NULL,
    list_id integer,
    tag_name character varying(200) DEFAULT NULL::character varying,
    style character varying(100) DEFAULT NULL::character varying
);
    DROP TABLE public.tr_tags;
       public         postgres    false    1826    1827    3            �            1259    17604    tr_tags_id_seq    SEQUENCE     p   CREATE SEQUENCE tr_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 %   DROP SEQUENCE public.tr_tags_id_seq;
       public       postgres    false    3    150            L           0    0    tr_tags_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE tr_tags_id_seq OWNED BY tr_tags.id;
            public       postgres    false    149            M           0    0    tr_tags_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('tr_tags_id_seq', 1, false);
            public       postgres    false    149            �            1259    17616    tr_users    TABLE     �   CREATE TABLE tr_users (
    id integer NOT NULL,
    username character varying(45) DEFAULT NULL::character varying,
    pass character varying(45) DEFAULT NULL::character varying
);
    DROP TABLE public.tr_users;
       public         postgres    false    1829    1830    3            �            1259    17614    tr_users_id_seq    SEQUENCE     q   CREATE SEQUENCE tr_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 &   DROP SEQUENCE public.tr_users_id_seq;
       public       postgres    false    152    3            N           0    0    tr_users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE tr_users_id_seq OWNED BY tr_users.id;
            public       postgres    false    151            O           0    0    tr_users_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('tr_users_id_seq', 6, true);
            public       postgres    false    151            �            1259    17626    tr_viewed_lists    TABLE     �   CREATE TABLE tr_viewed_lists (
    id integer NOT NULL,
    user_id integer,
    list_id integer,
    time_viewed timestamp without time zone,
    item_status text
);
 #   DROP TABLE public.tr_viewed_lists;
       public         postgres    false    3            �            1259    17624    tr_viewed_lists_id_seq    SEQUENCE     x   CREATE SEQUENCE tr_viewed_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
 -   DROP SEQUENCE public.tr_viewed_lists_id_seq;
       public       postgres    false    154    3            P           0    0    tr_viewed_lists_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE tr_viewed_lists_id_seq OWNED BY tr_viewed_lists.id;
            public       postgres    false    153            Q           0    0    tr_viewed_lists_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('tr_viewed_lists_id_seq', 1, true);
            public       postgres    false    153                       2604    17561    id    DEFAULT     d   ALTER TABLE ONLY tr_bookmarks ALTER COLUMN id SET DEFAULT nextval('tr_bookmarks_id_seq'::regclass);
 >   ALTER TABLE public.tr_bookmarks ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    141    140    141                       2604    17570    id    DEFAULT     d   ALTER TABLE ONLY tr_histories ALTER COLUMN id SET DEFAULT nextval('tr_histories_id_seq'::regclass);
 >   ALTER TABLE public.tr_histories ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    143    142    143                       2604    17579    id    DEFAULT     f   ALTER TABLE ONLY tr_item_lists ALTER COLUMN id SET DEFAULT nextval('tr_item_lists_id_seq'::regclass);
 ?   ALTER TABLE public.tr_item_lists ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    144    145    145                        2604    17601    id    DEFAULT     h   ALTER TABLE ONLY tr_list_shares ALTER COLUMN id SET DEFAULT nextval('tr_list_shares_id_seq'::regclass);
 @   ALTER TABLE public.tr_list_shares ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    148    147    148            !           2604    17609    id    DEFAULT     Z   ALTER TABLE ONLY tr_tags ALTER COLUMN id SET DEFAULT nextval('tr_tags_id_seq'::regclass);
 9   ALTER TABLE public.tr_tags ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    150    149    150            $           2604    17619    id    DEFAULT     \   ALTER TABLE ONLY tr_users ALTER COLUMN id SET DEFAULT nextval('tr_users_id_seq'::regclass);
 :   ALTER TABLE public.tr_users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    152    151    152            '           2604    17629    id    DEFAULT     j   ALTER TABLE ONLY tr_viewed_lists ALTER COLUMN id SET DEFAULT nextval('tr_viewed_lists_id_seq'::regclass);
 A   ALTER TABLE public.tr_viewed_lists ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    153    154    154            8          0    17558    tr_bookmarks 
   TABLE DATA               5   COPY tr_bookmarks (id, user_id, item_id) FROM stdin;
    public       postgres    false    141   �>       9          0    17567    tr_histories 
   TABLE DATA               G   COPY tr_histories (id, updated, created, user_id, item_id) FROM stdin;
    public       postgres    false    143   �>       :          0    17576    tr_item_lists 
   TABLE DATA               �   COPY tr_item_lists (id, title, owner_id, removed, created, updated, synced, token_expiry, current_token, token_seq) FROM stdin;
    public       postgres    false    145   �>       ;          0    17583    tr_items 
   TABLE DATA               �   COPY tr_items (id, content, parent_item_id, list_id, is_orphan, type, removed, is_hidden, updated, created, task_points, ref_item_id, image, task_status, synced, seq, id_seq, user_hash, remove_attempt) FROM stdin;
    public       postgres    false    146   %?       <          0    17598    tr_list_shares 
   TABLE DATA               7   COPY tr_list_shares (id, user_id, list_id) FROM stdin;
    public       postgres    false    148   �@       =          0    17606    tr_tags 
   TABLE DATA               8   COPY tr_tags (id, list_id, tag_name, style) FROM stdin;
    public       postgres    false    150   �@       >          0    17616    tr_users 
   TABLE DATA               /   COPY tr_users (id, username, pass) FROM stdin;
    public       postgres    false    152   �@       ?          0    17626    tr_viewed_lists 
   TABLE DATA               R   COPY tr_viewed_lists (id, user_id, list_id, time_viewed, item_status) FROM stdin;
    public       postgres    false    154   A       )           2606    17564    tr_bookmarks_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY tr_bookmarks
    ADD CONSTRAINT tr_bookmarks_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tr_bookmarks DROP CONSTRAINT tr_bookmarks_pkey;
       public         postgres    false    141    141            +           2606    17573    tr_histories_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY tr_histories
    ADD CONSTRAINT tr_histories_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.tr_histories DROP CONSTRAINT tr_histories_pkey;
       public         postgres    false    143    143            -           2606    17582    tr_item_lists_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY tr_item_lists
    ADD CONSTRAINT tr_item_lists_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.tr_item_lists DROP CONSTRAINT tr_item_lists_pkey;
       public         postgres    false    145    145            /           2606    17595    tr_items_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY tr_items
    ADD CONSTRAINT tr_items_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.tr_items DROP CONSTRAINT tr_items_pkey;
       public         postgres    false    146    146            1           2606    17603    tr_list_shares_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY tr_list_shares
    ADD CONSTRAINT tr_list_shares_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.tr_list_shares DROP CONSTRAINT tr_list_shares_pkey;
       public         postgres    false    148    148            3           2606    17613    tr_tags_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY tr_tags
    ADD CONSTRAINT tr_tags_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.tr_tags DROP CONSTRAINT tr_tags_pkey;
       public         postgres    false    150    150            5           2606    17623    tr_users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY tr_users
    ADD CONSTRAINT tr_users_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.tr_users DROP CONSTRAINT tr_users_pkey;
       public         postgres    false    152    152            7           2606    17634    tr_viewed_lists_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY tr_viewed_lists
    ADD CONSTRAINT tr_viewed_lists_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.tr_viewed_lists DROP CONSTRAINT tr_viewed_lists_pkey;
       public         postgres    false    154    154            8      x������ � �      9      x������ � �      :   F   x�3��K-�4���4204�50�52T04�26�20G3�20�21�4410�41�02�47i#�=... w�e      ;   Q  x����n� ���)���AE�O��AY+���/��66%m/
B����ϐf�J4�R&I��bk�a5[�[i��F��X7np:��"��3hFx��1%���`�x��,�x�,�̿��rS���"�̃�kyD�����\P���*=����L�4��A�V�70��fsx�u�f��J���%���,�@ÛB���Y������v��,��Mc�������2���RTܣ�\�q	r���6��'�:c5g��w�RPZ����$�Fu��crn��t��Y'�a�-�L�*3�Jn�
*6:�`3�=����X��浯*��FE�77AŁ��DQ�|�܃      <      x������ � �      =      x������ � �      >   G   x�3���L�K�442".#άԢ�Jט�$?�1��J,�σqM9�KR��sSaBf�)���0^� n!�      ?   S   x�3�4�?�j�̒��b%�j%���T�DC39?''��85E�*-1�8�VG�8>);#]���$i�o���]km-W� $�     