<?php

/**
 * !!!!! NOTICE !!!!!
 *
 * 本文件有经修改, 为了提高页面打开速度
 *
 */


/**
 * Less CSS Plugin
 *
 * Less Helper for compiling and minifying the outputted CSS and creating a
 * HTML link tag for css files.
 *
 * PHP Version 5.2
 *
 * @author Ben Auld <ben@benauld.com>
 * @copyright Copyright (c) 2012, Ben Auld. All rights reserved.
 * @package Less
 * @subpackage View.Helper
 */
App::import('Vendor', 'Less.LessPhp', array('file' => 'less.php.inc'));
App::import('Vendor', 'Less.CssMin', array('file' => 'cssmin-v3.0.1.php'));

class LessHelper extends AppHelper
{
	/**
	 * Helpers
	 *
	 * @var array
	 * @access public
	 */
	public $helpers = array(
		'Html'
	);

	/**
	 * Compiles and minifies supplied LESS files and returns HTML link tags. If
	 * Core debug is higher than 0, the cache is refreshed on each request,
	 * otherwise the cache is returned.
	 *
	 * Available options
	 * - minify (default: true)
	 *   If true, the compiled CSS will be minified
	 *
	 * - output_directory (default: ccss/)
	 *   Specifies where the the compiled CSS should live. This is a directory
	 *   within the webroot without the / at the beginning.
	 *
	 * @param mixed $path
	 * @param array $options
	 * @return string
	 * @access public
	 * @todo When debug is 0, don't recompile
	 */
	public function link($path, $options = array())
	{
		$defaults = array(
			'minify' => true,
			'output_directory' => 'ccss/',
			'appVersion' => 0
		);

		$options = array_merge($defaults, $options);

		if (is_string($path)) {
			$files = array($path);
		} else {
			$files = $path;
		}

		$outputPaths = array();
		foreach ($files as $k => $file) {
			if (strpos($file, '://') == false && substr($file, 0, 1) !== '/') {
				$files[$k] = '/css/' . $file;
			}

			if (strpos($files[$k], '://') == false) {
				$path = WWW_ROOT . substr($files[$k], 1);
			} else {
				$path = $files[$k];
			}

			$filename = md5($path . ':' . $options['appVersion']) . '.css';
			$outputPaths[] = '/' . $options['output_directory'] . $filename;
			$filepath = WWW_ROOT . $options['output_directory'] . $filename;

			if (file_exists($filepath) && strpos($options['appVersion'], 'dev-') === false) {
				continue;
			}

			$css = $this->__compileLess($path);

			if ($options['minify']) {
				$css = $this->__minifyCss($css);
			}

			file_put_contents($filepath, $css);
		}

		return $this->Html->css($outputPaths);
	}

	/**
	 * Invokes the LessPHP class and returns the compiled input
	 *
	 * @param string $path
	 * @return string
	 * @access private
	 */
	private function __compileLess($path)
	{
		$lessc = new lessc($path);
		return $lessc->parse();
	}

	/**
	 * Minifies given CSS
	 *
	 * @param string $input
	 * @return string
	 * @access public
	 */
	private function __minifyCss($input)
	{
		return CssMin::minify($input);
	}
}
